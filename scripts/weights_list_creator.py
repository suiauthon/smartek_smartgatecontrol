#!/usr/bin/env python2.7

import os, sys, glob

VERSION_MINOR = 0
VERSION_MAJOR = 1
VERSION_YEAR = "2018"
VERSION_MONTH = "07"
VERSION_DAY = "10"

def usage(program_name):
    print('{} WEIGHTS_DIR WEIGHTS_LIST_FILE [OPTIONS]'.format(program_name))
    print('Creates list of weights files that are located in given folder.\n')
    print(' WEIGHTS_DIR:            Full path to the folder with weights files.')
    print(' WEIGHTS_LIST_FILE:      Path to the file where the weights names will be saved.')
    print(' OPTIONS:')
    print('     -h  |   --help      Print a help message and exit.')
    print('     -V  |   --version   Display version information and exit.')

def version(program_name):
    print('{} {}.{} {}{}{}'.format(program_name, VERSION_MAJOR, VERSION_MINOR, VERSION_DAY, VERSION_MONTH, VERSION_YEAR))

def create_weights_list(weights_dir, list_name):
    if os.path.isfile(list_name):
        os.remove(list_name)
    file_list = open(list_name,'w')
    if not weights_dir.endswith('/'):
        weights_dir =  weights_dir + "/"
    weights = os.listdir(weights_dir)
    for weight in weights:
        if ".weights" in weight:
            file_list.write(weights_dir + weight + "\n")

if __name__ == "__main__":

    for i in range(len(sys.argv)):
        if (sys.argv[i] == '--help' or sys.argv[i] == '-h' or sys.argv[i] == '--version' or sys.argv[i] == '-V'):
            if (sys.argv[i] == '--help' or sys.argv[i] == '-h'):
                usage(sys.argv[0])
                sys.exit(0)
            elif (sys.argv[i] == '--version' or sys.argv[i] == '-V'):
                version(sys.argv[0])
            sys.exit(0)

    if (len(sys.argv) < 2):
        print('{}: missing  operand'.format(sys.argv[0]))
        print('Try {} --help or {} -h for more information.'.format(sys.argv[0], sys.argv[0]))
        sys.exit(0)
    elif (len(sys.argv) == 2):
        print('{}: wrong usage'.format(sys.argv[0]))
        print('Try {} --help or {} -h for more information.'.format(sys.argv[0], sys.argv[0]))
        sys.exit(0)


    weights_dir = sys.argv[1]
    list_name = sys.argv[2]

    create_weights_list(weights_dir, list_name)
