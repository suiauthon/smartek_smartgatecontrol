#!/usr/bin/env python2.7

import operator, sys, os

VERSION_MINOR = 0
VERSION_MAJOR = 1
VERSION_YEAR = "2018"
VERSION_MONTH = "07"
VERSION_DAY = "10"

def usage(program_name):
    print('{} RESULTS_DIR OUTPUT_FILE [OPTIONS]'.format(program_name))
    print('Creates list of results file sorted by precision/recall/IoU/mAP. Default is precision.\n')
    print(' RESULTS_DIR:            Full path to the folder with weights files.')
    print(' OUTPUT_FILE:            File where sorted path to results will be stored.')
    print(' OPTIONS:')
    print('     -p  |   --precision     Sort by precision.')
    print('     -r  |   --recall        Sort by recall.')
    print('     -mAP                    Sort by mean average precision (mAP).')
    print('     -iou                    Sort by IoU.')
    print('     -FP                     Sort by false positive.')
    print('     -TP                     Sort by true positive.')
    print('     -FN                     Sort by false negative.')
    print('     -h  |   --help          Print a help message and exit.')
    print('     -V  |   --version       Display version information and exit.')

def version(program_name):
    print('{} {}.{} {}{}{}'.format(program_name, VERSION_MAJOR, VERSION_MINOR, VERSION_DAY, VERSION_MONTH, VERSION_YEAR))


def read_result(result_file, mAP, precision, recall, iou, FP, TP, FN):
    result = open(result_file,'r')
    for line in result:
        if ("mean average precision" in line):
            value = float(line.split(',')[1].replace(' ','').replace('or','').replace('%',''))
            mAP[result_file] = value
        elif ("precision" in line):
            value = float(line.split(',')[1].split('=')[1].replace(' ',''))
            precision[result_file] = value
            value = float(line.split(',')[2].split('=')[1].replace(' ',''))
            recall[result_file] = value
        elif ("IoU" in line):
            value = float(line.split(',')[4].split('=')[1].replace(' ','').replace('%',''))
            iou[result_file] = value

            value = float(line.split(',')[1].split('=')[1].replace(' ',''))
            TP[result_file] = value

            value = float(line.split(',')[2].split('=')[1].replace(' ',''))
            FP[result_file] = value

            value = float(line.split(',')[3].split('=')[1].replace(' ',''))
            FN[result_file] = value
    result.close()


def read_results_from_dir(results_dir, mAP, precision, recall, iou, FP, TP, FN):
    results = os.listdir(results_dir)
    for result in results:
        read_result(results_dir+result, mAP, precision, recall, iou, FP, TP, FN)

def save_dict_to_file(file, dict):
    file_list = open(file,'w')
    for key, value in sorted(dict.items(), key=operator.itemgetter(1)):
        file_list.write(str(value) + "     " + key + "\n")
    file_list.close()


if __name__ == "__main__":
    for i in range(len(sys.argv)):
        if (sys.argv[i] == '--help' or sys.argv[i] == '-h' or sys.argv[i] == '--version' or sys.argv[i] == '-V'):
            if (sys.argv[i] == '--help' or sys.argv[i] == '-h'):
                usage(sys.argv[0])
                sys.exit(0)
            elif (sys.argv[i] == '--version' or sys.argv[i] == '-V'):
                version(sys.argv[0])
            sys.exit(0)

    if (len(sys.argv) < 2):
        print('{}: missing  operand'.format(sys.argv[0]))
        print('Try {} --help or {} -h for more information.'.format(sys.argv[0], sys.argv[0]))
        sys.exit(0)
    elif (len(sys.argv) == 2):
        print('{}: wrong usage'.format(sys.argv[0]))
        print('Try {} --help or {} -h for more information.'.format(sys.argv[0], sys.argv[0]))
        sys.exit(0)

    mAP = {}
    precision = {}
    recall = {}
    iou = {}
    FP = {}
    TP = {}
    FN = {}

    read_results_from_dir(sys.argv[1], mAP, precision, recall, iou, FP, TP, FN)

    if (len(sys.argv) > 3):
        if (sys.argv[3] == '-p' or sys.argv[3] == '--precision'):
            save_dict_to_file(sys.argv[2], precision)
        elif (sys.argv[3] == '-r' or sys.argv[3] == '--recall'):
            save_dict_to_file(sys.argv[2], recall)
        elif (sys.argv[3] == '-mAP'):
            save_dict_to_file(sys.argv[2], mAP)
        elif (sys.argv[3] == '-iou'):
            save_dict_to_file(sys.argv[2], iou)
        elif (sys.argv[3] == '-FP'):
            save_dict_to_file(sys.argv[2], FP)
        elif (sys.argv[3] == '-TP'):
            save_dict_to_file(sys.argv[2], TP)
        elif (sys.argv[3] == '-FN'):
            save_dict_to_file(sys.argv[2], FN)
        else:
            print('{}: wrong usage'.format(sys.argv[0]))
            print('Try {} --help or {} -h for more information.'.format(sys.argv[0], sys.argv[0]))
            sys.exit(0)
    else:
        save_dict_to_file(sys.argv[2], precision)