#!/usr/bin/env python3

#from imagecluster import main as cluster
from PIL import Image
import operator
from shutil import copyfile
import os, sys, glob

def rename(path, index, extension):
    current_index = index
    filenames = os.listdir(path)
    for filename in filenames:
        if (os.path.isdir(path+filename)):
            current_index = rename(path+filename+"/", current_index, extension)
        elif (os.path.isfile(path+filename)):
            os.rename(path+filename,path+str(current_index)+"."+extension)
            current_index = current_index + 1

    return current_index

def delete_file_trough_sym_link(folder_path):
    filenames = os.listdir(folder_path)
    for filename in filenames:
        if (os.path.isdir(folder_path+filename)):
            delete_file_trough_sym_link(folder_path+filename+"/")
            os.rmdir(folder_path+filename+"/")
        elif (os.path.isfile(folder_path+filename)):
            os.remove(os.path.realpath(folder_path+filename))
            os.remove(folder_path+filename)

    #os.rmdir(folder_path)

def delete_all_except_bigest_in_folder(folder_path):
    filenames = os.listdir(folder_path)
    filename_to_keep = ""
    size_of_file_to_keep = 0
    for filename in filenames:
        if (os.path.isdir(folder_path+filename)):
            delete_all_except_bigest_in_folder(folder_path+filename+"/")
        elif (os.path.isfile(folder_path+filename)):
            width, height = Image.open(os.path.realpath(folder_path+filename)).size
            if (size_of_file_to_keep < width*height):
                size_of_file_to_keep = width*height
                filename_to_keep = filename

    for filename in filenames:
        if (os.path.isfile(folder_path+filename)):
            if (filename != filename_to_keep):
                os.remove(os.path.realpath(folder_path+filename))
                os.remove(folder_path+filename)

def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    if (h > 1):
        h = 1
    if (w > 1):
        w = 1
    return (x,y,w,h)

def merge_labels(folder_path1, folder_path2, output_folder, images_path, list_path):
    filenames1 = os.listdir(folder_path1)
    filenames2 = os.listdir(folder_path2)
    list = open(list_path+"train_smartek.txt",'w')
    for filename in filenames1:
        fd1 = open(folder_path1+filename,'r')
        file = open(output_folder+filename,'w')
        line_number = 0
        for line in fd1:
            line_number = line_number + 1
            if line_number > 1:
                width, height = Image.open(images_path+filename.split('.')[0]+".jpg").size

                elems = line.split(' ')
                xmin = elems[0]
                xmax = elems[2]
                ymin = elems[1]
                ymax = elems[3]
                b = (float(xmin), float(xmax), float(ymin), float(ymax))
                bb = convert((width,height), b)
                file.write(str(0) + " " + " ".join([str(a) for a in bb]) + '\n')

        if filename in filenames2:
            fd2 = open(folder_path2+filename,'r')
            line_number = 0
            for line in fd2:
                file.write(line)
            fd2.close()

        list.write(images_path+filename.split('.')[0]+".jpg"+'\n')
        fd1.close()
        file.close()

    list.close()

def filter_only_person(image_path, label_path, image_out, label_out, list_out):
    images = os.listdir(image_path)
    labels = os.listdir(label_path)
    list = open(list_out+"voc_labels.txt",'w')
    for label in labels:
        flag = 0
        f_label = open(label_path+label,'r')
        for line in f_label:
            elems = line.split(' ')
            if (int(elems[0]) == 14):
                flag = 1
        f_label.seek(0)
        if flag:
            f_label_out = open(label_out+label, 'w')
            iterator = 0
            for line in f_label:
                elems = line.split(' ')
                if (int(elems[0]) == 14):
                    iterator = iterator +1
                    f_label_out.write(line)
                    print(iterator)

            f_label_out.close()
            copyfile(image_path+label.split('.')[0]+".jpg", image_out+label.split('.')[0]+".jpg")
            list.write(image_out+label.split('.')[0]+".jpg"+'\n')
        f_label.close()
    list.close()

def get_all_except_person(image_path, label_path, image_out, label_out):
    labels = os.listdir(label_path)
    for label in labels:
        flag = 0
        f_label = open(label_path+label,'r')
        for line in f_label:
            elems = line.split(' ')
            if (int(elems[0]) == 14):
                flag = 1
        f_label.seek(0)
        if not flag:
            f_label_out = open(label_out+label, 'w')
            f_label_out.close()
            copyfile(image_path+label.split('.')[0]+".jpg", image_out+label.split('.')[0]+".jpg")
        f_label.close()

def change_id(label_path, label_out):
    labels = os.listdir(label_path)
    for label in labels:
        f_label = open(label_path+label,'r')
        f_label_out = open(label_out+label,'w')
        for line in f_label:
            elems = line.split(' ')
            if (int(elems[0]) == 14):
                elems[0] = "1"
            line1 = elems[0]+" "+elems[1]+" "+elems[2] +" "+elems[3]+" "+elems[4]+"\n"
            f_label_out.write(line1)
        f_label_out.close()
        f_label.close()

def create_train_and_val_set(image_dir, output_dir, percentage_test):
    file_train = open(output_dir + "train.txt", 'w')
    file_test = open(output_dir + "test.txt",'w')
    counter = 1
    index_test = round(100 / percentage_test)
    for pathAndFilename in glob.iglob(os.path.join(image_dir, "*.jpg")):
        title, ext = os.path.splitext(os.path.basename(pathAndFilename))
        if counter == index_test:
            counter = 1
            file_test.write(image_dir + title + '.jpg' + "\n")
        else:
            file_train.write(image_dir + title + '.jpg' + "\n")
            counter = counter + 1

def create_weights_list(weights_dir, output_dir):
    file_list = open(output_dir + "MC2018-tiny_6_608_weights_list.txt",'w')
    weights = os.listdir(weights_dir)
    for weight in weights:
        file_list.write(weights_dir + weight + "\n")

def move_empty(src_dir_label, src_dir, out_dir):
    labels = os.listdir(src_dir_label)
    for label in labels:
        f_label = open(src_dir_label+label,'r')
        ima = 0
        for line in f_label:
            elems = line.split(' ')
            if (elems[0] != '\n'):
                ima = 1
        if (not ima):
            copyfile(src_dir+label.split('.')[0]+".jpg", out_dir+label.split('.')[0]+".jpg")
            copyfile(src_dir_label+label.split('.')[0]+".txt", out_dir+label.split('.')[0]+".txt")
            os.remove(src_dir+label.split('.')[0]+".jpg")
            os.remove(src_dir_label+label.split('.')[0]+".txt")

def read_result(result_file, mAP, precision, recall, iou):
        result = open(result_file,'r')
        for line in result:
            if ("mean average precision" in line):
                value = float(line.split(',')[1].replace(' ','').replace('or','').replace('%',''))
                mAP[result_file] = value
            elif ("precision" in line):
                value = float(line.split(',')[1].split('=')[1].replace(' ',''))
                precision[result_file] = value
                value = float(line.split(',')[2].split('=')[1].replace(' ',''))
                recall[result_file] = value
            elif ("IoU" in line):
                value = value = float(line.split(',')[4].split('=')[1].replace(' ','').replace('%',''))
                iou[result_file] = value


def read_results_from_dir(results_dir, mAP, precision, recall, iou):
    results = os.listdir(results_dir)
    for result in results:
        read_result(results_dir+result, mAP, precision, recall, iou)

def save_dict_to_file(file, dict):
    file_list = open(file,'w')
    for key, value in sorted(dict.items(), key=operator.itemgetter(1)):
        file_list.write(key + "     " + value + "\n")

if __name__ == "__main__":
    i = 1
    sim = 0.75
    path1 = "/home/suiauthon/smartek_ws/object_detection/dataset/forklift/Labels/000/"
    path2 = "/home/suiauthon/smartek_ws/object_detection/dataset/forklift/Labels/001/"
    path3 = "/home/suiauthon/smartek_ws/object_detection/dataset/forklift/Labels/"
    path4 = "/home/suiauthon/smartek_ws/object_detection/dataset/forklift/JPEGImages/"
    path5 = "/home/suiauthon/smartek_ws/object_detection/dataset/forklift/"
    voc_labels = "/home/suiauthon/smartek_ws/object_detection/dataset/VOCdevkit/VOC2012/labels/"
    voc_images = "/home/suiauthon/smartek_ws/object_detection/dataset/VOCdevkit/VOC2012/JPEGImages/"
    voc_labels_out = "/home/suiauthon/smartek_ws/object_detection/dataset/voc_label/"
    voc_images_out = "/home/suiauthon/smartek_ws/object_detection/dataset/voc/"
    #list_out = "/home/suiauthon/smartek_ws/object_detection/dataset/"
    labels = "/home/suiauthon/smartek_ws/object_detection/dataset/labels/"
    labels1 = "/home/suiauthon/smartek_ws/object_detection/dataset/labels1/"
    weights_dir = "/home/suiauthon/smartek_ws/object_detection/weights/MC2018-tiny_6_608/"
    weights_list_dir = "/home/suiauthon/smartek_ws/object_detection/weights/"
    result_file = "/home/suiauthon/smartek_ws/object_detection/results/MC2018-tiny/"

    #delete_all_except_bigest_in_folder(path)

    #delete_file_trough_sym_link(path)

    #rename(path, 1, "jpg")

    #cluster.main(path, sim)
    mAP = {}
    precision = {}
    recall = {}
    iou = {}
    read_results_from_dir(result_file,mAP, precision, recall, iou)
    save_dict_to_file(result_file, mAP)
    save_dict_to_file(result_file, mAP)
    #print(mAP)
    #print(precision)
    #print(recall)
    #print(iou)

    #merge_labels(path1, path2, path3, path4, path5)
    #filter_only_person(voc_images, voc_labels, voc_images_out, voc_labels_out, list_out)
    #change_id(labels, labels1)
    #create_train_and_val_set("/home/suiauthon/smartek_ws/object_detection/dataset/NYU/JPEGImages/", "/home/suiauthon/smartek_ws/object_detection/dataset/NYU/")
    #get_all_except_person(voc_images, voc_labels, voc_images_out, voc_labels_out)
    #create_weights_list(weights_dir, weights_list_dir)
    #move_empty("/home/suiauthon/smartek_ws/object_detection/dataset/MC2018/labels/", "/home/suiauthon/smartek_ws/object_detection/dataset/MC2018/JPEGImages/", "/home/suiauthon/smartek_ws/object_detection/dataset/MC2018/novo/")
