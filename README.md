# SMARTEK_SmartGateControl

## Darknet installation
First make sure that you have CUDA and CUDNN installed. If yes proceeed with compiling darknet otherwise please install CUDA and CUDNN. To compile darknet just do `make` in the darknet directory. Before make, you have to set options in the `Makefile`:

* `GPU = 1` to build with CUDA
* `CUDNN = 1` to build with CUDNN

It is not necessary to build darknet with opencv and openmp so you can do following:

* `OPENMP = 0` to build without OPENMP
* `OPENCV = 0` to build without OPENCV

## Data annotation
To train our own object we have to compose own training set. To do so, use the Yolo_mark Tool to annotate the training images. This is a c++ library which can be clone from the GitHub repository:

```
git@github.com:suiauthon/Yolo_mark.git
```

Create a file with extension `.names` and inside that file write names of the object that you wish to annotate.
After that follow the instruction given on this repository to annotate custom data set.

## Neural network train
To train neural network first we have to configure our network.

1. Create file with custom name and '.cfg' extension. If you are using yolo3 or yolo2 layer then you have to follow some rules:
    * change line batch to `batch=64`
    * change line subdivisions to `subdivisions=8`
    * change number of classes to your number of objects
    * for yolo3 change filters size in the last convolutional layer before yolo layer to: `filters=(classes + 5)x3`  
        (Generally `filters` depends on the `classes`, `coords` and number of `mask`s, i.e. filters=`(classes + coords + 1)*<number of mask>`,   
        where `mask` is indices of anchors. If `mask` is absence, then filters=`(classes + coords + 1)*num`).

    Examples of `.cfg` files are given in folder `cfg`.

2. Create file with custom name and `.data` extension. This file should contain following:

    `classes = 2`  
    `train = data/train.txt`  
    `valid = data/test.txt`  
    `names = data/MC2018.names`  
    `backup = backup/`  
    `results = results/`   

    where:

    * `classes` - number of objects.
    * `train` - path to the list with train images.
    * `valid` - path to the list with validation images.
    * `names` - path to the file with names of the objects.
    * `backup` - path to the folder where weights will be saved during neural network training.
    * `results` - path to the folder where results of the validation will be saved.

3. Download pre-trained weights for the convolutional layers.
    * For `yolov3-voc.cfg` download weights `darknet53.conv.74` from `link`.
    * For `yolov3.cfg` download weights `darknet53.conv.74` from `link`.
    * For `yolov3-tiny.cfg` download weights `yolov3-tiny.conv.15` from `link`.

    If you made you custom model that isn't based on other models, then you can train it without pre-trained weights, then will be used random initial weights.

4. Start training by using the command:
    `./darknet detector train cfg/*.data cfg/*.cfg pre-trained_weights`

    where:

    * `cfg/*.data` - path to `.data` file that contain data as explained.
    * `cfg/*.cfg` - path to neural network configuration file.
    * `pre-trained_weights` - path to pre-trained weights

**Note:** If during training you see `nan` values for `avg` (loss) field - then training goes wrong, but if `nan` is in some other lines - then training goes well.

**Note:** If you changed width= or height= in your cfg-file, then new width and height must be divisible by 32.

**Note:** if error `Out of memory` occurs then in `.cfg`-file you should increase `subdivisions=16`, 32 or 64.
    
### When to stop training
Usually sufficient 2000 iterations for each class(object). But for a more precise definition when you should stop training, use the following manual:

1. When you see that average loss 0.xxxxxx avg no longer decreases at many iterations then you should stop training.

2. Once training is stopped, you should take some of last .weights-files from `\backup` and choose the best of them.  
    For example, you stopped training after 9000 iterations, but the best result can give one of previous weights (7000, 8000, 9000).  
    It can happen due to overfitting. **Overfitting** - is case when you can detect objects on images from training-dataset, but can't detect ojbects on any others images. You should get weights from **Early Stopping Point**:

    ![Overfitting](https://hsto.org/files/5dc/7ae/7fa/5dc7ae7fad9d4e3eb3a484c58bfc1ff5.png)
    
### How to validate neural network
When training is compleate you have to create weights list in order to validate your training network and to find best weights. To create weights list use `weights_list_creator.py` from `script` folder.
You can use this script with following command:

```
./weights_list_creator.py WEIGHTS_DIR WEIGHTS_LIST_FILE
```

where:

* `WEIGHTS_DIR` - is full path to the folder with weights files.
* `WEIGHTS_LIST_FILE` - is path to the file where the weights names will be saved.

In your *.cfg file set `batch=1` and `subdivisions=1`, and use this settings while working with already trained network. To validate your trained network use validator with command:

```
./validator DATACFG CFGFILE WEIGHTSFILE
```

where:

* `DATACFG` - is path to config file that containst names and number of classes.
* `CFGFILE` - is path to neural network architecture config file.
* `WEIGHTSFILE` - is path to weights list file.
    
The validation result will be saved in to folder provided in `.data` file under the argument `results`. After validation one can get sorted results using script `results_sorter.py` from `script` folder.

```
./results_sorter.py RESULTS_DIR OUTPUT_FILE [OPTIONS] 
```

Choose weights-file with the highest IoU (intersect of union) and mAP (mean average precision).

### Tips and tricks
* set flag `random=1` in your .cfg-file - it will increase precision by training Yolo for different resolutions.
* increase network resolution in your .cfg-file (height=608, width=608 or any value multiple of 32) - it will increase precision
* desirable that your training dataset include images with objects at diffrent: scales, rotations, lightings, from different sides
* desirable that your training dataset include images with objects (without labels) that you do not want to detect - negative samples
* for training with a large number of objects in each image, add the parameter `max=200` or higher value in the yolo layer (default is 90). 
* If you train the model to distinguish Left and Right objects as separate classes (left/right hand, left/right-turn on road signs, ...) then for disabling flip data augmentation - add `flip=0`.
* recalculate anchors for your dataset for `width` and `height` from cfg-file:
  `calculate_anchors data/obj.data -num_of_clusters 9 -width 416 -height 416`
   then set the same 9 `anchors` in each of 3 `[yolo]`-layers in your cfg-file

## Installation instructions

Gate control is easy to install with few commands:

```
mkdir build
cd build
cmake ..
make
```

after installation you can start gate control with:

```
./SMARTEK_SmartGateControl DATACFG CFGFILE WEIGHTFILE FRONT_CAM_VIDEO BACK_CAM_VIDEO
```
