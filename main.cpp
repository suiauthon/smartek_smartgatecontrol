#include <iostream>
#include <main.h>
#include <unistd.h>
#include <fstream>
#include <unistd.h>
#include <sstream>
#include <vector>
#include <layer.h>
#include <Detector.h>

void matIntoImage(cv::Mat* src, image im)
{
    unsigned char *data = (unsigned char *)src->data;
    int h = src->rows;
    int w = src->cols;
    int c = src->channels();
    int i, j, k, color_index = 0, index;

    for (k = 0; k < c; k++) {
        color_index = (c - k + 2) % c;
        for (j = 0; j < h; j++) {
            for (i = 0; i < w; i++) {
                index = i + w*j + w*h*k;
                im.data[index] = (float)data[color_index + (i + w*j) * c] / 255.0;
            }
        }
    }
}

void imageIntoMat(image im, cv::Mat* src)
{
    unsigned char *data = (unsigned char *)im.data;
    src->rows = im.h;
    src->cols = im.w;
    int h = src->rows;
    int w = src->cols;
    int c = src->channels();
    int i, j, k, color_index = 0, index;
    for (k = 0; k < c; k++) {
        color_index = (c - k + 2) % c;
        for (j = 0; j < h; j++) {
            for (i = 0; i < w; i++) {
                index = i + w*j + w*h*k;
                src->data[color_index + (i + w*j) * c] = im.data[index] * 255;
            }
        }
    }
}


int main(int argc, char **argv) {
    int gpu_i = -1;
    Detector::objects *o, *o2;
    Detector::object det;
    image im, resized_im, im2;
    double time_begining, time_grab, time_predict, time;
    int i = 0, w, h, c;
    float *data = NULL;
    int rate;
    unsigned int microseconds;

    if(argc < 2) {
        fprintf(stderr, "usage: %s <function>\n", argv[0]);
        return 0;
    }

    gpu_i = find_int_arg(argc, argv, "-i", 0);
    if(find_arg(argc, argv, "-nogpu")) {
        gpu_i = -1;
    }

#ifndef GPU
    gpu_i = -1;
#endif

    /*Grabber grabber;
    grabber.findDevices();
    grabber.connect(0);

    Detector d(argv[1], gpu_i);
    d.loadNetwork(argv[2], argv[3]);
    //d.initializePredictionFilter(3);

    time=what_time_is_it_now();

    while (what_time_is_it_now() - time < 120) {

        time_begining=what_time_is_it_now();
        data = grabber.grab(0, w, h, c);
        time_grab = what_time_is_it_now()-time_begining;

        if (data != NULL)
        {
            printf("Image grabbed in %f seconds. \n",time_grab);

            //convert image to image struct
            im = float_to_image(w, h, 3, data);

            resized_im = resize_image(im, 760, 600);

            o = d.predict(resized_im);
            printf("%d \n", o.num);
            for (int j = 0; j < o.num; j++) {
                printf("%s: %.2f \n", o.name[j], o.prob[j]);
                printf("%.2f %.2f %.2f %.2f \n", o.b[j].x, o.b[j].y, o.b[j].w, o.b[j].h);
            }

            if (o.num > 0) {
                free(o.b);
                free(o.prob);
                free(o.name);
            }

            free_image(im);
            free_image(resized_im);
        }

        printf("Image processed in %f seconds. \n \n",what_time_is_it_now()-time_begining);

        rate = (0.5 - (what_time_is_it_now()-time_begining))*1000000;

        if (rate<= 0) rate = 1;

        usleep(rate);
        i++;
        printf("Time with sleep in %f seconds. \n \n",what_time_is_it_now()-time_begining);
    }

    grabber.disconect(0);*/

    //jedna slika
    /*im = load_image_color(argv[4],0,0);

    Detector d(argv[1]);
    d.loadNetwork(argv[2], argv[3], gpu_i);
    d.initializePredictionFilter(1);

    for (int i = 0; i<1; i++) {
        time=what_time_is_it_now();
        o = d.predict(im);
        printf("Time predict in %f seconds. \n \n",what_time_is_it_now()-time);
        printf("%d \n", o->num);
        for (int j = 0; j < o->num; j++) {
            printf("%s: %.2f \n", o->name[j], o->prob[j]);
            printf("%f %f %f %f \n", o->b[j].x, o->b[j].y, o->b[j].w, o->b[j].h);

            det.b.x = o->b[j].x;
            det.b.y = o->b[j].y;
            det.b.h = o->b[j].h;
            det.b.w = o->b[j].w;
            d.drawDetection(im, &det,"person1");

        }
        if (o->num > 0) d.freeObjects(o);
    }
    save_image(im,"prediction");
    free_image(im);*/

    /*Detector d(argv[1], gpu_i);
    d.initialize_network(argv[2], argv[3]);

    std::ifstream input("/home/suiauthon/smartek_ws/object_detection/dataset/forklift/train_smartek.txt" );

    for( std::string line; getline( input, line ); )
    {
        std::ofstream labels_f;
        im = load_image_color((char *)line.c_str(),0,0);
        o = d.predict(im);
        std::vector<std::string> name;
        char del = '/';
        name = split(line,del);
        std::vector<std::string> name1;
        del = '.';
        int flag1 = 0;
        name1 = split(name[name.size()-1], del);
        if (o.num > 0) {
            for (int i = 0; i < o.num; i++) {
                if (!std::string(o.name[i]).compare(std::string("person"))) {
                    flag1 = 1;
                }
            }
        }
        if (flag1)
        {
            std::string file = "/home/suiauthon/smartek_ws/object_detection/dataset/forklift/Labels/" + name1[0]+".txt";
            labels_f.open(file.c_str());
        }
        for (int i = 0; i < o.num; i++) {
            if (!std::string(o.name[i]).compare(std::string("person"))) {
                labels_f << "1 "<<o.b[i].x<<" "<<o.b[i].y<<" "<<o.b[i].w<<" "<<o.b[i].h<<"\n";
            }
        }
        if ( flag1>0 ) labels_f.close();
        free_image(im);
    }*/

    //video file
    Detector d(argv[1]), d2(argv[1]);
    Clusterer cl, cl2;
    GateCommander gate_commander;
    box b1, b2;
    b1.x = 0.5;
    b1.y = 0.8;
    b1.w = 1;
    b1.h = 0.4;

    b2.x = 0.5;
    b2.y = 0.8;
    b2.w = 1;
    b2.h = 0.4;
    gate_commander.setHotZone(b1, b2);
    gate_commander.setCheckMovementDirection(false);
    gate_commander.setGateInFrontZone(0.5, 1, 1.57, 1, 0.01);
    gate_commander.setGateInBackZone(0.5, 1, 1.57, 1, 0.01);
    gate_commander.setMaxTimeInMiddleEarth(15);
    list *l, *l2;

    d.loadNetwork(argv[2], argv[3], gpu_i);
    d2.loadNetwork(argv[2], argv[3], gpu_i);
    d.initializePredictionFilter(3);
    d2.initializePredictionFilter(3);
    printf("video file: %s\n", argv[4]);
    cv::VideoCapture cap(argv[4]);
    cv::VideoCapture cap2(argv[5]);

    cv::Mat frame, frame_out;
    cv::Mat frame2, frame_out2;

    if(!cap.isOpened()) error("Couldn't open video file.\n");
    if(!cap2.isOpened()) error("Couldn't open video file.\n");

    cv::Size S = cv::Size((int) cap.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
                  (int) cap.get(CV_CAP_PROP_FRAME_HEIGHT));

    cv::VideoWriter outputVideo1("front_cam.avi",CV_FOURCC('M','J','P','G'),10, S);
    cv::VideoWriter outputVideo2("back_cam.avi",CV_FOURCC('M','J','P','G'),10, S);


    cv::namedWindow( "w", 1);
    cv::namedWindow( "w2", 1);

    cap >> frame;
    cap2 >> frame2;
    frame_out = frame.clone();
    frame_out2 = frame2.clone();
    im = make_image(frame.cols, frame.rows, frame.channels());
    im2 = make_image(frame2.cols, frame2.rows, frame2.channels());
    for( ; ; )
    {
        i++;
        cap >> frame;
        cap2 >> frame2;
        if(frame.empty() || frame2.empty())
            break;

        matIntoImage(&frame, im);
        matIntoImage(&frame2, im2);
        //time=what_time_is_it_now();
        o = d.predict(im);
        o2 = d2.predict(im2);
        det.b = b1;
        d.drawDetection(im, det.b,(char*)"front hot_zone", Detector::Blue, 0.006, 0.02);

        det.b = b2;
        d.drawDetection(im2, det.b,(char*)"back hot_zone", Detector::Blue, 0.006, 0.02);
        /*for (int j=0; j<o->num; j++)
        {
            det.b.x = o->b[j].x;
            det.b.y = o->b[j].y;
            det.b.h = o->b[j].h;
            det.b.w = o->b[j].w;

            //d.drawDetection(im, &det,"person1");
        }*/
        //printf("Predicted in %f seconds.\n",what_time_is_it_now()-time);
        cl.updateModel(0.03);
        cl2.updateModel(0.03);
        cl.updateMeasurement(o, 0.03);
        cl2.updateMeasurement(o2, 0.03);
        l = cl.getCluster("person");
        l2 = cl2.getCluster("person");
        //printf("Velicina liste %d\n", l->size);
        node *n = l->front;
        node *n2 = l2->front;
        //if (!n) printf("0,0,");
        while(n) {
            Detector::object *ol = (Detector::object*)n->val;
            char id[3];
            char ime[20] = "person_";
            sprintf(id, "%d", ol->id);
            //printf("%f,%f, %f, %f, %f, %f,",ol->vb.x, ol->b.x, ol->vz, ol->z, ol->vb.y, ol->b.y);
            strcat(ime, id);
            d.drawDetection(im, ol->b,ime, Detector::Red, 0.006, 0.02);
            n = n->next;
        }
        while(n2) {
            Detector::object *ol = (Detector::object*)n2->val;
            char id[3];
            char ime[20] = "person_";
            sprintf(id, "%d", ol->id);
            printf("Id %s\n", id);
            //printf("%f,%f, %f, %f, %f, %f,",ol->vb.x, ol->b.x, ol->vz, ol->z, ol->vb.y, ol->b.y);
            strcat(ime, id);
            d.drawDetection(im2, ol->b,ime, Detector::Red, 0.006, 0.02);
            n2 = n2->next;
        }
        printf("\n");
        gate_commander.insertObjects(l,l2);
        char front_zone_obj[50], back_zone_obj[50];
        sprintf(front_zone_obj,"Number of objects: %d", gate_commander.getNumberOfObjectsInFrontZone());
        sprintf(back_zone_obj,"Number of objects: %d", gate_commander.getNumberOfObjectsInBackZone());
        printf("Broj ljudi front hot_zone %d\n", gate_commander.getNumberOfObjectsInFrontZone());
        printf("Broj ljudi back hot_zone %d\n", gate_commander.getNumberOfObjectsInBackZone());
        printf("Middle earth: %d\n", gate_commander.getNumberOfObjectsInMiddleEarth());
        printf("Command: %d\n", gate_commander.getCommand());
        free_list_contents(l);
        free_list(l);
        free_list_contents(l2);
        free_list(l2);
        /*printf("%d \n", o->num);
        for (int j = 0; j < o->num ; j++) {
            //if (o->num > j) {
            //    printf("%f, %f, %f, %f, %f, ", o->prob[j], o->b[j].x, o->b[j].y, o->b[j].w, o->b[j].h);
            //}
            //else printf("0, 0, 0, 0, 0, ");
            printf("%s: %.2f \n", o->name[j], o->prob[j]);
            //printf("%.2f %.2f %.2f %.2f \n", o->b[j].x, o->b[j].y, o->b[j].w, o->b[j].h);
        }
        printf("\n");*/
        if (o->num > 0) d.freeObjects(o);
        if (o2->num > 0) d2.freeObjects(o2);

        imageIntoMat(im, &frame_out);
        imageIntoMat(im2, &frame_out2);

        if (gate_commander.getCommand()) {
            cv::putText(frame_out, std::string("Gate: OPEN"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,255,0,255),2);
            cv::putText(frame_out2, std::string("Gate: OPEN"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,255,0,255),2);
        }
        else {
            cv::putText(frame_out, std::string("Gate: CLOSE"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,0,255,255),2);
            cv::putText(frame_out2, std::string("Gate: CLOSE"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,0,255,255),2);
        }
        cv::putText(frame_out, front_zone_obj, cv::Point2f(100,130), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,0,0,255),2);
        cv::putText(frame_out2, back_zone_obj, cv::Point2f(100,130), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,0,0,255),2);
        //putText(mbgra, str, Point2f(100,100), FONT_HERSHEY_PLAIN, 2,  ));
        //cv::putText(frame_out, const string& text, Point org, int fontFace, double fontScale, Scalar color);

        outputVideo1 << frame_out;
        outputVideo2 << frame_out2;
        cv::imshow("w", frame_out);
        cv::imshow("w2", frame_out2);
        cv::waitKey(1);
    }
    //cv::waitKey(0); // key press to close window
    free_image(im);
    free_image(im2);

    //clusterer
    /*Clusterer cccc;
    Detector::objects *o;
    int i;
    GateCommander gate_commander;
    box b1;
    b1.x = 0.5;
    b1.y = 0.5;
    b1.w = 1.0;
    b1.h = 1.0;
    gate_commander.setHotZone(b1);
    list *l;
    char a[7] = "person";
    o = (Detector::objects*)malloc(sizeof(Detector::objects));
    o->num = 1;
    o->prob = (float*)malloc(sizeof(float));
    o->prob[0] = 0;
    o->b = (box*)malloc(sizeof(box));
    o->b[0].x = 0.5;
    o->b[0].y = 0.5;
    o->b[0].w = 0.2;
    o->b[0].h = 0.3;
    o->name = (char**)malloc(sizeof(char*));
    o->name[0] = a;
    for (i=0; i<500; i++) {
        cccc.updateModel(0.03);
        cccc.updateMeasurement(o, 0.03);
        l = cccc.getCluster("person");
        printf("%d\n",l->size);
        gate_commander.insertObjects(l);
        printf("Broj ljudi %d\n", gate_commander.getNumberOfObjects());
        free_list_contents(l);
        free_list(l);
        //printf("Person\n");
        usleep(3000);
    }
    free(o->b);
    free(o->prob);
    free(o->name);
    free(o);*/

    return 0;
}