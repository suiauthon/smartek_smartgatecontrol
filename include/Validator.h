//
// Created by suiauthon on 12.07.18..
//

#ifndef OBJECT_DETECTION_VALIDATOR_H
#define OBJECT_DETECTION_VALIDATOR_H

#define VERSION_MAJOR   (int)1
#define VERSION_MINOR   (int)0
#define VERSION_YEAR    ((const unsigned char *)"2018")
#define VERSION_MONTH   ((const unsigned char *)"07")
#define VERSION_DAY     ((const unsigned char *)"12")

#include <Detector.h>

#endif //OBJECT_DETECTION_VALIDATOR_H
