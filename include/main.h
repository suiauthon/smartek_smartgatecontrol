//
// Created by suiauthon on 20.03.18..
//

#ifndef OBJECT_DETECTION_MAIN_H
#define OBJECT_DETECTION_MAIN_H

#include <Detector.h>
#include <Grabber.h>
#include <Clusterer.h>
#include <GateCommander.h>
#include <opencv2/opencv.hpp>
#include <pthread.h>

#define GRAB_RATE (double)0.05
#define DETECTION_RATE (double)0.05
#define PROCESS_RATE (double)0.03

#endif //OBJECT_DETECTION_MAIN_H
