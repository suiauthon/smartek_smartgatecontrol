/**
 * @class GateCommander
 * @brief A Class that intelligently decides whether to open or close a door.
 * The decision is taken with respect to object position, door position and
 * hot zone position in an image. Also, it is possible to consider the movement
 * direction of the object and made decision based on that.
 *
 * @details
 * Contact: carek.marko@gmail.com
 *
 * @author Marko Car
 * @version 0.1
 * @date July 2018
 *
 */

#ifndef OBJECT_DETECTION_GATECOMMANDER_H
#define OBJECT_DETECTION_GATECOMMANDER_H

#include <darknet.h>
#include <MedianFilter.h>

extern "C" {
#include "utils.h"
}

class GateCommander {
    public:

        /**
         * @brief A constructor.
         * Initializes all private variables.
         */
        GateCommander(void);

        /**
         * @brief A destructor.
         * Frees all used memory.
         */
        ~GateCommander(void);

        /**
         * @brief A method that returns state (open/closed) of the door.
         *
         * @return bool value true if door should be open or false if door should be closed.
         */
        bool getCommand(void);

        /**
         * @brief A method that returns filtered state (open/closed) of the door.
         * The median filter with size set with method setMaxTimeInMiddleEarth() is used.
         *
         * @return bool value true if door should be open or false if door should be closed.
         */
        bool getFilteredCommand(void);

        /**
         * @brief A method that set if movement direction of object should be included in decision.
         *
         * @param check_movement_direction parameter set to true if should include objects movement
         * direction in decision or false if not.
         */
        void setCheckMovementDirection(bool check_movement_direction);

        /**
         * @brief Sets a location of the hot zones in images.
         *
         * @param front location of the hot zone in front image.
         * @param back location of the hot zone in back image.
         */
        void setHotZone(box front, box back);

        /**
         * @brief sets a position of a gate in front image.
         *
         * @param x position of gate in image - x axis.
         * @param y position of gate in image - y axis.
         * @param orientation orientation of the gate in rad.
         * @param w width of a gate.
         * @param h height of a gate.
         */
        void setGateInFrontZone(float x, float y, float orientation, float w, float h);

        /**
         * @brief sets a position of a gate in back image.
         *
         * @param x position of gate in image - x axis.
         * @param y position of gate in image - y axis.
         * @param orientation orientation of the gate in rad.
         * @param w width of a gate.
         * @param h height of a gate.
         */
        void setGateInBackZone(float x, float y, float orientation, float w, float h);

        /**
         * @brief A method that returns number of objects in front zone.
         *
         * @return number of objects in front zone.
         */
        int getNumberOfObjectsInFrontZone(void);

        /**
         * @brief A method that returns number of objects in back zone.
         *
         * @return number of objects in back zone.
         */
        int getNumberOfObjectsInBackZone(void);

        /**
         * @brief this method inserts objects in corresponding lists and checks if some of objects are
         * in front or back zone or in middle earth. If parameter with method setCheckMovementDirection() is set
         * to true this method also checks if object has a movement direction towards the door or against the door.
         *
         * @param front_l list of the objects detected in front image.
         * @param back_l list of the objects detected in back image.
         */
        void insertObjects(list *front_l, list *back_l);

        /**
         * @brief A method that returns number of objects located in middle earth (place between cameras
         *
         * @return number of objects located in middle earth (place between cameras
         */
        int getNumberOfObjectsInMiddleEarth(void);

        /**
         * @brief Sets a size of the median filter.
         *
         * @param size size of the median filter.
         */
        void setCommandMedianFilterSize(int size);

        /**
         * @brief Sets parameter for maximal time after which object will be removed from middle earth list.
         *
         * @param max_time after that time object will be removed from middle earth list.
         */
        void setMaxTimeInMiddleEarth(double max_time);

private:
        typedef struct {
            bool in_zone_x_right;
            bool in_zone_x_left;
            bool in_zone_y_top;
            bool in_zone_y_bot;
            bool going_towards_gate;
            float x;
            float y;
            float vx;
            float vy;
            int id;
        } object;

        typedef struct {
            float x;
            float y;
            float direction;
            float height;
            float width;
        } gate;

        enum side {
            Right,
            Bot,
            Left,
            Top,
            Unknown
        };

        void insertObject(list *l, list *objects, int id, float x, float y, float vx, float vy, box hot_zone, gate gate_pos);
        GateCommander::side getSideFromDirection(float direction, float w, float h);
        GateCommander::side getExitSide(GateCommander::object *obj, GateCommander::object *obj_old);
        GateCommander::side getEntranceSide(GateCommander::object *obj, GateCommander::object *obj_old);
        GateCommander::object *popObjectById(list *l, int id);
        bool isGoingTowardsGate(float vx, float vy, float x, float y, gate gate_pos, float velocity_direction_thresh, float position_thresh);
        bool isNearGate(float x, float y, box hot_zone, gate gate_pos, float thresh);
        bool hysteresis(bool y, float x, float T, float center);
        double wrapTo2PI(double angle);
        bool is_hot_zone_initialized_, check_speed_, command_;
        bool is_gate_position_in_front_set_, is_gate_position_in_back_set_;
        int objects_in_middle_earth_;
        float hysteresis_T_, zone_thresh_, velocity_direction_thresh_, velocity_distance_thresh_;
        double middle_earth_update_time_, max_time_in_middle_earth_;
        float filtered_command_;
        box front_hot_zone_, back_hot_zone_;

        list *front_zone_objects_, *back_zone_objects_;
        gate gate_position_in_front_, gate_position_in_back_;
        MedianFilter command_median_filter_;
};


#endif //OBJECT_DETECTION_GATECOMMANDER_H
