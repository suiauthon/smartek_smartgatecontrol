/**
 * @class MovingAverage
 * @brief Implementation of the moving average filter.
 *
 * Moving average is a type of finite impulse response filter. It is a mathematical result that
 * is calculated by averaging a number of past data points.
 *
 * @details
 * Contact: carek.marko@gmail.com
 *
 * @author Marko Car
 * @version 0.1
 * @date June 2018
 *
 */

#ifndef OBJECT_DETECTION_MOVINGAVERAGE_H
#define OBJECT_DETECTION_MOVINGAVERAGE_H

#include <stdlib.h>

class MovingAverage {
    public:
        /**
         * @brief A constructor.
         * It initializes all the necessary variables.
         */
        MovingAverage(void);

        /**
         * @brief A destructor.
         * Frees all used memory.
         */
        ~MovingAverage(void);

        /**
         * @brief It allocates the necessary memory for the given filter size.
         *
         * @param size The size of the filter
         */
        void setSize(int size);

        /**
         * @brief A method that puts new data into array and returns the new, filtered data.
         *
         * @param data The data that needs to be filtered.
         * @return  Filtered data.
         */
        float filter(float data);

    private:
        int filter_size_, data_size_, index_;
        float *data_;
};


#endif //OBJECT_DETECTION_MOVINGAVERAGE_H
