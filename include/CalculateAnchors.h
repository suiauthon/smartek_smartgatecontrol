#ifndef GATECONTROL_CALCULATEANCHORS_H
#define GATECONTROL_CALCULATEANCHORS_H

#include <darknet.h>
#include <opencv2/opencv.hpp>

extern "C" {
#include "utils.h"
}

typedef struct {
    float w, h;
} anchors_t;

#define VERSION_MAJOR   (int)1
#define VERSION_MINOR   (int)0
#define VERSION_YEAR    ((const unsigned char *)"2018")
#define VERSION_MONTH   ((const unsigned char *)"08")
#define VERSION_DAY     ((const unsigned char *)"15")

#endif //GATECONTROL_CALCULATEANCHORS_H
