/**
 * @class Clusterer
 * @brief A Class that devides input objects in to a cluster. It also filter all the object's measurement using
 * kalman filter from \ref KalmanFilter class.
 *
 * @details
 * Contact: carek.marko@gmail.com
 *
 * @author Marko Car
 * @version 0.1
 * @date June 2018
 *
 */

#ifndef OBJECT_DETECTION_CLUSTERER_H
#define OBJECT_DETECTION_CLUSTERER_H

#include <Detector.h>
#include <KalmanFilter.h>
#include <MovingAverage.h>

class Clusterer {
    public:
        /**
         * @brief A constructor.
         * Initializes all private variables.
         */
        Clusterer(void);

        /**
         * @brief A destructor.
         * Frees all used memory.
         */
        ~Clusterer(void);

        /**
         * @brief This method updates all the objects in clusters using kalman filter prediction phase.
         * It updates each object position, box width, box height. Also this method removes object
         * from cluster if the object's measurements are not updated in the last max_delay seconds.
         *
         * @param dt Time passed since last update.
         */
        void updateModel(double dt);

        /**
         * @brief This method put all objects, that are not already in a cluster, in to a cluster. If the object is
         * already in a cluster, this method then updates its measurements (position, box width, box height).
         * Each cluster is a type of a object ("forklift", "person" ...). To determine if object is already in cluster,
         * the closest object to input object by distance in cluster is found. Then the cluster is searched again
         * to find the object whose detection box fits best to input object's detection box. This measure is called iou
         * (Intersection over Union). If the iou is larger than min_iou or if the distance divided by dt is smaller than
         * distance_thresh then the input object corresponds to the found object. If so, object measurement are updated
         * and for each measurement the kalman filter correction phase is performed. If the object is not in the cluster
         * then this object is simply added to the cluster.
         *
         * @param obj Structure that contains all the objects detected in a single pass.
         * @param dt Time passed since last update.
         */
        void updateMeasurement(Detector::objects *obj, double dt);

        /**
         * @brief Sets the size of the moving average filter for velocity.
         *
         * @param size Size of the moving average filter.
         */
        void setVelocityMovingAverageFilterSize(int size);

        /**
         * @brief Sets the time after which the object is removed from the cluster if object's measurements
         * are not updated.
         *
         * @param max_delay Time in seconds.
         */
        void setMaxDelay(float max_delay);

        /**
         * @brief Sets the minimum time which object must to be in the cluster to take that object in to account.
         *
         * @param min_time Time in seconds.
         */
        void setMinTime(float min_time);

        /**
         * @brief Sets the minimum iou.
         *
         * @param min_iou Float value in range from 0 to 1.
         */
        void setMinIou(float min_iou);

        /**
         * @brief This methot returns the cluster by its name. It exclude all the objects which time in cluster is lower
         * than min_time. Objects in list are type of Detector::Object.
         *
         * @param key The name of the cluster.
         * @return List that contains object from wanted cluster.
         */
        list *getCluster(char *key);

    private:
        typedef struct {
            char *key;
            list *val;
        } classCluster;

        typedef struct objectFilter {
            KalmanFilter x, y, z, w, h;
            MovingAverage mx, my, mz, mw, mh;
            bool updated;
            double time_s;
            double time_u;
            objectFilter():
                x(),
                y(),
                z(),
                w(),
                h(),
                mx(),
                my(),
                mz(),
                mw(),
                mh(){
                }
        } objectFilter;

        typedef struct {
            Detector::object o;
            objectFilter *filt;
        } clusterObject;

        void insertObject(list *l, char *key, Clusterer::clusterObject *val);
        float calculateDistance(Clusterer::clusterObject *x, Clusterer::clusterObject *y);
        Clusterer::clusterObject *findClosestObject(list *l, Clusterer::clusterObject *target, float *dist);
        Clusterer::clusterObject *findMaxIouObject(list *l, Clusterer::clusterObject *target, float *max_iou);
        Clusterer::clusterObject *findObject(list *l, Clusterer::clusterObject *obj, char *key, double dt);
        void updateObjectFilterMeasurement(Clusterer::clusterObject *o);
        void updateObjectFilterModel(Clusterer::clusterObject *o, double dt);
        float estimateObjectZ(float x);
        void setObjectId(list *l, Clusterer::clusterObject *obj);
        list *findClassCluster(list *l, char *key);
        void initializeObjectFilter(Clusterer::clusterObject *o);
        void insertObjects(Detector::objects *obj, double dt);
        void removeNode(list *l, node *n);
        void freeClusters(list *l);

        float max_delay_, min_time_, min_iou_;
        int velocity_moving_average_filter_size_;
        list *clusters_;

};


#endif //OBJECT_DETECTION_CLUSTERER_H
