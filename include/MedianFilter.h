/**
 * @class MedianFilter
 * @brief Implementation of the median filter.
 *
 * The main idea of the median filter is to run through the signal entry by entry, replacing each entry
 * with the median of neighboring entries. The pattern of neighbors is called the "window", which slides,
 * entry by entry, over the entire signal. The window is the first few preceding entries and current entry.
 * The window must have an odd number of entries. Then the median is the middle value after all the entries
 * in the window are sorted numerically.
 *
 * For example if we have a window of 5 elements: [2, 8, 3, 6, 7]. First the array is sorted: [2, 3, 6, 7, 8].
 * Then the middle value (number 6) is taken as filtered value.
 *
 * @details
 * Contact: carek.marko@gmail.com
 *
 * @author Marko Car
 * @version 0.1
 * @date July 2018
 *
 */

#ifndef OBJECT_DETECTION_MEDIANFILTER_H
#define OBJECT_DETECTION_MEDIANFILTER_H

#include <stdint.h>

class MedianFilter{
    public:
        /**
         * @brief A constructor.
         * It initializes all the necessary variables.
         */
        MedianFilter();

        /**
         * @brief It allocates the necessary memory for the given filter size.
         *
         * @param size The size of the filter. Size has to be odd number.
         */
        void init(uint32_t size);

        /**
         * @brief A method that do magic. It filters given data.
         *
         * @param data The data that needs to be filtered.
         * @return Filtered data.
         */
        float filter(float data);

        /**
        * @brief A destructor.
        * Frees all used memory.
        */
        ~MedianFilter();

    private:
        float *measurements_;
        bool is_initialized_;
        uint32_t size_;
};

#endif //OBJECT_DETECTION_MEDIANFILTER_H
