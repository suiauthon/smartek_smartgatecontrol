/**
 * @class Detector
 * @brief A Class that detect objects in a image. Is uses neural network implementation from darknet library.
 *
 * @details
 * Contact: carek.marko@gmail.com
 *
 * @author Marko Car
 * @version 0.1
 * @date March 2018
 *
 */

#ifndef OBJECT_DETECTION_DETECTOR_H
#define OBJECT_DETECTION_DETECTOR_H

#include <darknet.h>

extern "C" {
    #include "utils.h"
}

class Detector {
    public:
        /**
        * @brief Object data struct which contain its position, velocity, probability and id.
        */
        typedef struct object {
            box b; /**< Position of the box that defines the object.*/
            box vb; /**< Velocity of the box that defines the object.*/
            float z; /**< Distance in z direction of object from camera*/
            float vz; /**< Velocity of the object in z direction.*/
            float prob; /**< Probability of the object.*/
            int id; /**< Object's id.*/
        } object;

        /**
         * @brief List of implemented colors
         */
        enum colors {
            Blue, /**< Blue color.*/
            Orange, /**< Orange color.*/
            Yellow, /**< Yellow color.*/
            Purple, /**< Purple color.*/
            Green, /**< Green color.*/
            Turquoise, /**< Turquoise color.*/
            Red /**< Red color.*/
        };

        /**
         * @brief Objects data struct which contain name, probability, location of each object,
         * and number of detected objects in the image.
         */
        typedef struct {
            object *obj; /**< Pointer to object data array.*/
            box *b; /**< Pointer to objects box data array.*/
            char **name; /**< Pointer to object names array.*/
            float *prob; /**< Pointer to objects probability array.*/
            int num; /**< Number of objects in array.*/
        } objects;

        /**
         * @brief A constructor.
         * It loads names and number of classes and initializes all the necessary variables.
         *
         * @param datacfg path to config file that containst names and number of classes.
         */
        Detector(char *datacfg);

        /**
         * @brief A destructor.
         * Frees all used memory.
         */
        ~Detector(void);

        /**
         * @brief Loads neural network based on config file and provided weights.
         *
         * @param cfgfile path to neural network architecture config file.
         * @param weightfile path to neural network weights file
         * @param gpu_i GPU card id that will be used to detect objects.
         * @return returns 1 if network is successfully loaded otherwise returns 0.
         */
        int loadNetwork(char *cfgfile, char *weightfile, int gpu_i);

        /**
         * @brief Set value for confidence threshold. Default value is 0.3.
         *
         * @param thresh new value of confidence threshold
         */
        void setConfidenceThreshold(float thresh);

        /**
         * @brief Set value of non-maximum suppression. Default value is 0.45.
         *
         * @param nms new value of non-maximum suppression.
         */
        void setNms(float nms);

        /**
         * @brief Set value of the hier threshold. Default value is 0.5.
         *
         * @param hier_thresh new value of hier threshold
         */
        void setHierThresh(float hier_thresh);

        /**
         * @brief Predicts objects name and location with some probability on image.
         *
         * @param im the image which has to be processed.
         * @return objects on image in form @ref objects structure.
         */
        Detector::objects* predict(image im);

        /**
         * @brief A method that initialize prediction filter.
         *
         * @param filter_size size of the filter
         * @return returns 1 if filter is successfully initialized, otherwise returns 0.
         */
        int initializePredictionFilter(int filter_size);

        /**
         * @brief Frees objects structure
         *
         * @param  obj objects structure that needs to be freed
         */
        static void freeObjects(objects *obj);

        /**
         * @brief A method for training detector
         *
         * @param  cfgfile path to neural network architecture config file.
         * @param  weightfile path to neural network weights file
         * @param  gpus list of the gpus that can be used to train neural network
         * @param  ngpus number of gpus that can be used to train neural network
         * @param  clear set to 1 if network should be trained from start
         */
        void train(char *cfgfile, char *weightfile, int *gpus, int ngpus, int clear);

        /**
         * @brief A method that validates loaded network
         *
         * @param outfile path to the file where the validation results will be saved
         * @param thresh_calc_avg_iou parameter that defines the detection probability threshold
         */
        void validate(char *outfile, float thresh_calc_avg_iou);

        /**
         * @brief This method frees used network from memory
         */
        void freeNetwork(void);

        /**
         * @brief A method that loads given weights into the network
         * @param weightfile path to the network weights
         */
        void loadWeights(char *weightfile);

        /**
         * @brief This method draws the box that defines the object on given image
         *
         * @param im image on which the box will be drawn
         * @param obj the object which will be drawn on image
         * @param name the name that should be displayed above box
         * @param color the color of the displayed box
         * @param thickness the thickness of the box line
         * @param font_size the size of the font
         */
        void drawDetection(image im, box b, char *name, Detector::colors color, float thickness, float font_size);

        /**
         * @brief This method makes copy of the objects
         *
         * @param obj the pointer to the objects struct that needs to be copied
         * @return pointer to the new objects struct
         */
        static Detector::objects *copyObjects(objects *obj);

        int getNetworkWidth(void);
        int getNetworkHeight(void);

    private:
        typedef struct {
            box b;
            float p;
            int class_id;
            int image_index;
            int truth_flag;
            int unique_truth_index;
        } box_prob;

        typedef struct {
            double precision;
            double recall;
            int tp, fp, fn;
        } pr_t;

        Detector::objects *getDetectedObjects(detection *dets, int num, float thresh, char **names, int classes);
        void rememberNetwork(network *net, int index, float **predictions);
        void avgPredictions(network *net, int net_size, float *avg, float **predictions, int filter_size);
        static int detectionsComparator(const void *pa, const void *pb);
        int networkSize(network *net);

        int is_prediction_filter_initialized_, actual_prediction_filter_size_;
        int prediction_filter_index_, prediction_filter_size_, is_network_loaded_;
        int net_size_, classes_;
        char **names_, *name_list_, *train_images_, *backup_directory_;
        char *valid_images_, *results_directory_;
        float confidence_threshold_, **predictions_, *avg_;
        float hier_thresh_, nms_;

        list *options_;
        image  **alphabet_;
        network *net_;
};

#endif //OBJECT_DETECTION_DETECTOR_H
