#include <CalculateAnchors.h>


void calc_anchors_find_replace(char *str, char *orig, char *rep, char *output) {
    char *buffer = (char*) calloc(8192, sizeof(char));
    char *p;

    sprintf(buffer, "%s", str);
    if(!(p = strstr(buffer, orig))){  // Is 'orig' even in 'str'?
        sprintf(output, "%s", str);
        free(buffer);
        return;
    }

    *p = '\0';

    sprintf(output, "%s%s%s", buffer, rep, p+strlen(orig));
    free(buffer);
}

void calc_anchors_find_replace_extension(char *str, char *orig, char *rep, char *output) {
    char *buffer = (char*) calloc(8192, sizeof(char));

    sprintf(buffer, "%s", str);
    char *p = strstr(buffer, orig);
    int offset = (p - buffer);
    int chars_from_end = strlen(buffer) - offset;
    if (!p || chars_from_end != strlen(orig)) {  // Is 'orig' even in 'str' AND is 'orig' found at the end of 'str'?
        sprintf(output, "%s", str);
        free(buffer);
        return;
    }

    *p = '\0';

    sprintf(output, "%s%s%s", buffer, rep, p + strlen(orig));
    free(buffer);
}

int anchors_comparator(const void *pa, const void *pb)
{
    anchors_t a = *(anchors_t *)pa;
    anchors_t b = *(anchors_t *)pb;
    float diff = b.w*b.h - a.w*a.h;
    if (diff < 0) return 1;
    else if (diff > 0) return -1;
    return 0;
}

void calc_anchors_replace_image_to_label(char *input_path, char *output_path) {
    //find_replace(input_path, "/images/", "/labels/", output_path);    // COCO
    calc_anchors_find_replace(input_path, "/images/train2014/", "/labels/train2014/", output_path);    // COCO
    calc_anchors_find_replace(output_path, "/images/val2014/", "/labels/val2014/", output_path);        // COCO
    calc_anchors_find_replace(output_path, "/JPEGImages/", "/labels/", output_path);    // PascalVOC
    calc_anchors_find_replace(output_path, "\\images\\train2014\\", "\\labels\\train2014\\", output_path);    // COCO
    calc_anchors_find_replace(output_path, "\\images\\val2014\\", "\\labels\\val2014\\", output_path);        // COCO
    calc_anchors_find_replace(output_path, "\\JPEGImages\\", "\\labels\\", output_path);    // PascalVOC
    //find_replace(output_path, "/VOC2007/JPEGImages/", "/VOC2007/labels/", output_path);        // PascalVOC
    //find_replace(output_path, "/VOC2012/JPEGImages/", "/VOC2012/labels/", output_path);        // PascalVOC

    //find_replace(output_path, "/raw/", "/labels/", output_path);

    // replace only ext of files
    calc_anchors_find_replace_extension(output_path, ".jpg", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".JPG", ".txt", output_path); // error
    calc_anchors_find_replace_extension(output_path, ".jpeg", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".JPEG", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".png", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".PNG", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".bmp", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".BMP", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".ppm", ".txt", output_path);
    calc_anchors_find_replace_extension(output_path, ".PPM", ".txt", output_path);
}

void calc_anchors(char *datacfg, int num_of_clusters, int width, int height) {
    printf("\n num_of_clusters = %d, width = %d, height = %d \n", num_of_clusters, width, height);
    if (width < 0 || height < 0) {
        printf("Usage: calculate_anchors data/voc.data -num_of_clusters 9 -width 416 -height 416 \n");
        printf("Error: set width and height \n");
        return;
    }

    //float pointsdata[] = { 1,1, 2,2, 6,6, 5,5, 10,10 };
    float *rel_width_height_array = (float*) calloc(1000, sizeof(float));

    list *options = read_data_cfg(datacfg);
    char *train_images = option_find_str(options, "train", "data/train.list");
    list *plist = get_paths(train_images);
    int number_of_images = plist->size;
    char **paths = (char **)list_to_array(plist);

    int number_of_boxes = 0;
    printf(" read labels from %d images \n", number_of_images);

    int i, j;
    for (i = 0; i < number_of_images; ++i) {
        char *path = paths[i];
        char labelpath[4096];
        calc_anchors_replace_image_to_label(path, labelpath);

        int num_labels = 0;
        box_label *truth = read_boxes(labelpath, &num_labels);
        //printf(" new path: %s \n", labelpath);
        char buff[1024];
        for (j = 0; j < num_labels; ++j)
        {
            if (truth[j].x > 1 || truth[j].x <= 0 || truth[j].y > 1 || truth[j].y <= 0 ||
                truth[j].w > 1 || truth[j].w <= 0 || truth[j].h > 1 || truth[j].h <= 0)
            {
                printf("\n\nWrong label: %s - j = %d, x = %f, y = %f, width = %f, height = %f \n",
                       labelpath, j, truth[j].x, truth[j].y, truth[j].w, truth[j].h);
                sprintf(buff, "echo \"Wrong label: %s - j = %d, x = %f, y = %f, width = %f, height = %f\" >> bad_label.list",
                        labelpath, j, truth[j].x, truth[j].y, truth[j].w, truth[j].h);
                system(buff);
            }
            number_of_boxes++;
            rel_width_height_array = (float*) realloc(rel_width_height_array, 2 * number_of_boxes * sizeof(float));
            rel_width_height_array[number_of_boxes * 2 - 2] = truth[j].w * width;
            rel_width_height_array[number_of_boxes * 2 - 1] = truth[j].h * height;
            printf("\r loaded \t image: %d \t box: %d", i+1, number_of_boxes);
        }
    }
    printf("\n all loaded. \n");

    CvMat* points = cvCreateMat(number_of_boxes, 2, CV_32FC1);
    CvMat* centers = cvCreateMat(num_of_clusters, 2, CV_32FC1);
    CvMat* labels = cvCreateMat(number_of_boxes, 1, CV_32SC1);

    for (i = 0; i < number_of_boxes; ++i) {
        points->data.fl[i * 2] = rel_width_height_array[i * 2];
        points->data.fl[i * 2 + 1] = rel_width_height_array[i * 2 + 1];
        //cvSet1D(points, i * 2, cvScalar(rel_width_height_array[i * 2], 0, 0, 0));
        //cvSet1D(points, i * 2 + 1, cvScalar(rel_width_height_array[i * 2 + 1], 0, 0, 0));
    }


    const int attemps = 10;
    double compactness;

    enum {
        KMEANS_RANDOM_CENTERS = 0,
        KMEANS_USE_INITIAL_LABELS = 1,
        KMEANS_PP_CENTERS = 2
    };

    printf("\n calculating k-means++ ...");
    // Should be used: distance(box, centroid) = 1 - IoU(box, centroid)
    cvKMeans2(points, num_of_clusters, labels,
              cvTermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 10000, 0), attemps,
              0, KMEANS_PP_CENTERS,
              centers, &compactness);

    // sort anchors
    qsort(centers->data.fl, num_of_clusters, 2*sizeof(float), anchors_comparator);

    printf("\n");
    float avg_iou = 0;
    for (i = 0; i < number_of_boxes; ++i) {
        float box_w = points->data.fl[i * 2];
        float box_h = points->data.fl[i * 2 + 1];
        //int cluster_idx = labels->data.i[i];
        int cluster_idx = 0;
        float min_dist = FLT_MAX;
        for (j = 0; j < num_of_clusters; ++j) {
            float anchor_w = centers->data.fl[j * 2];
            float anchor_h = centers->data.fl[j * 2 + 1];
            float w_diff = anchor_w - box_w;
            float h_diff = anchor_h - box_h;
            float distance = sqrt(w_diff*w_diff + h_diff*h_diff);
            if (distance < min_dist) min_dist = distance, cluster_idx = j;
        }

        float anchor_w = centers->data.fl[cluster_idx * 2];
        float anchor_h = centers->data.fl[cluster_idx * 2 + 1];
        float min_w = (box_w < anchor_w) ? box_w : anchor_w;
        float min_h = (box_h < anchor_h) ? box_h : anchor_h;
        float box_intersect = min_w*min_h;
        float box_union = box_w*box_h + anchor_w*anchor_h - box_intersect;
        float iou = box_intersect / box_union;
        if (iou > 1 || iou < 0) { // || box_w > width || box_h > height) {
            printf(" Wrong label: i = %d, box_w = %d, box_h = %d, anchor_w = %d, anchor_h = %d, iou = %f \n",
                   i, box_w, box_h, anchor_w, anchor_h, iou);
        }
        else avg_iou += iou;
    }
    avg_iou = 100 * avg_iou / number_of_boxes;
    printf("\n avg IoU = %2.2f %% \n", avg_iou);

    char buff[1024];
    FILE* fw = fopen("anchors.txt", "wb");
    printf("\nSaving anchors to the file: anchors.txt \n");
    printf("anchors = ");
    for (i = 0; i < num_of_clusters; ++i) {
        sprintf(buff, "%2.4f,%2.4f", centers->data.fl[i * 2], centers->data.fl[i * 2 + 1]);
        printf("%s", buff);
        fwrite(buff, sizeof(char), strlen(buff), fw);
        if (i + 1 < num_of_clusters) {
            fwrite(", ", sizeof(char), 2, fw);
            printf(", ");
        }
    }
    printf("\n");
    fclose(fw);

    free(rel_width_height_array);
    cvReleaseMat(&points);
    cvReleaseMat(&centers);
    cvReleaseMat(&labels);
}

void usage(char* progName)
{
    std::cout << progName << " DATACFG [OPTIONS]" << std::endl <<
              "Calculate anchors for given width and height." << std::endl <<
              "" << std::endl <<
              "    DATACFG:                 Path to config file that containst names and number of classes." << std::endl <<
              "    OPTIONS:" << std::endl <<
              "      -num_of_clusters       Number of clusters. (mandatory)" << std::endl <<
              "      -width                 Width of an image. (mandatory)" << std::endl <<
              "      -height                Height of an image. (mandatory)" << std::endl <<
              "      -i                     Use a specific GPU. (default: 0)" << std::endl <<
              "      -h | --help            Print a help message and exit." << std::endl <<
              "      -V | --Version         Display version information and exit." << std::endl;


}

void version(char* progName)
{
    std::cout << progName << " "<< VERSION_MAJOR << "." << VERSION_MINOR << " " << VERSION_DAY << VERSION_MONTH << VERSION_YEAR << std::endl;
}

int main(int argc, char **argv) {
    char *datacfg;
    int num_of_clusters, width, height;

    if (find_arg(argc, argv, (char*)"-h") || find_arg(argc, argv, (char*)"--help")) {
        usage(argv[0]);
        return 0;
    }
    else if (find_arg(argc, argv, (char*)"-V") || find_arg(argc, argv, (char*)"--version")) {
        version(argv[0]);
        return 0;
    }
    else if(argc < 2) {
        fprintf(stderr, "%s: missing  operand\n", argv[0]);
        fprintf(stderr, "Try '%s --help' or '%s -h' for more information.\n", argv[0], argv[0]);
        return 0;
    }
    else if (argc < 8){
        fprintf(stderr, "%s: wrong usage\n", argv[0]);
        fprintf(stderr, "Try '%s --help' or '%s -h' for more information.\n", argv[0], argv[0]);
        return 0;
    }
    else {
        datacfg = argv[1];
        num_of_clusters = find_int_arg(argc, argv, "-num_of_clusters", 5);
        width = find_int_arg(argc, argv, "-width", -1);
        height = find_int_arg(argc, argv, "-height", -1);

        std::string temp1(argv[1]);

        if (temp1.find(std::string(".data")) == std::string::npos) {
            fprintf(stderr, "%s: wrong usage\n", argv[0]);
            fprintf(stderr, "Try '%s --help' or '%s -h' for more information.\n", argv[0], argv[0]);
            return 0;
        }
    }

    calc_anchors(datacfg, num_of_clusters, width, height);

    return 0;
}

