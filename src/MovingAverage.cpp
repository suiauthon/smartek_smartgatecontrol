#include "MovingAverage.h"

MovingAverage::MovingAverage(void):
    filter_size_(60),
    data_size_(0),
    index_(0) {
    //Allocating the memory for the initial filter length
    data_ = (float*)malloc(filter_size_*sizeof(float));
}

MovingAverage::~MovingAverage(void) {
    free(data_);
}

void MovingAverage::setSize(int size) {
    filter_size_ = size;

    //Reallocation of the memory
    data_ = (float*)realloc(data_, filter_size_*sizeof(float));
}

float MovingAverage::filter(float data) {
    int i;
    float sum = 0;

    //Adding new data to the filter array.
    //We have a cyclic buffer, and at the end
    //the average of the whole buffer/array is
    //returned.
    data_[index_] = data;
    index_++;
    index_ = index_ % filter_size_;
    if (data_size_ < filter_size_) data_size_++;

    for (i = 0; i < data_size_; i++) sum += data_[i];

    return (float)(sum/data_size_);
}