#include <MedianFilter.h>
#include <algorithm>

MedianFilter::MedianFilter(void):
    is_initialized_(false) {

}

void MedianFilter::init(uint32_t size) {
    if (is_initialized_) delete[] measurements_;

    if (size % 2 != 0)
        size_ = size;
    else
        size_ = 1;

    measurements_ = new float[size_];

    for (uint32_t i = 0; i < size_; i++) measurements_[i] = 0;

    is_initialized_ = true;
}

MedianFilter::~MedianFilter() {
    delete[] measurements_;
}

float MedianFilter::filter(float data) {
    int i = 0;
    float *sorted_measurements_;
    float filtered_data = 0;

    if (is_initialized_) {
        sorted_measurements_ = new float[size_];

        for (i = size_ - 1; i > 0; i--) {
            measurements_[i] = measurements_[i - 1];
        }
        measurements_[0] = data;

        std::copy(measurements_, measurements_ + size_, sorted_measurements_);

        std::sort(sorted_measurements_, sorted_measurements_ + size_);

        filtered_data = sorted_measurements_[int(size_ / 2)];

        delete[] sorted_measurements_;
    }

    return filtered_data;
}