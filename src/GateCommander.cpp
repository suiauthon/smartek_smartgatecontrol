//
// Created by suiauthon on 21.05.18..
//
#include <cmath>
#include <layer.h>
#include <GateCommander.h>
#include <Detector.h>

GateCommander::GateCommander(void):
        is_hot_zone_initialized_(false),
        check_speed_(false),
        command_(false),
        is_gate_position_in_front_set_(false),
        is_gate_position_in_back_set_(false),
        objects_in_middle_earth_(0),
        hysteresis_T_(0.02),
        max_time_in_middle_earth_(2.0),
        zone_thresh_(0.05),
        velocity_direction_thresh_(0.9),
        velocity_distance_thresh_(0.05),
        filtered_command_(0.0) {
    front_zone_objects_ = make_list();
    back_zone_objects_ = make_list();

    command_median_filter_.init(9);

    middle_earth_update_time_ = what_time_is_it_now();
}

GateCommander::~GateCommander(void) {
    free_list_contents(front_zone_objects_);
    free_list(front_zone_objects_);
    free_list_contents(back_zone_objects_);
    free_list(back_zone_objects_);
}

void GateCommander::insertObjects(list *front_l, list *back_l) {
    if (is_hot_zone_initialized_ && is_gate_position_in_back_set_ &&
            is_gate_position_in_front_set_) {
        node *front_n = front_l->front;
        node *back_n = back_l->front;
        node *front_zone_n, *back_zone_n;
        list *front_temp_objects, *back_temp_objects;

        front_temp_objects = make_list();
        back_temp_objects = make_list();

        while (front_n) {
            auto *o = (Detector::object *) front_n->val;

            insertObject(front_temp_objects, front_zone_objects_, o->id, o->b.x, 1-o->z, o->vb.x, -o->vz,
                         front_hot_zone_, gate_position_in_front_);

            front_n = front_n->next;
        }

        front_zone_n = front_zone_objects_->front;
        while (front_zone_n) {
            auto *o = (GateCommander::object*) front_zone_n->val;

            if (o->in_zone_x_left && o->in_zone_x_right && o->in_zone_y_bot && o->in_zone_y_top) {
                if (isNearGate(o->x, o->y, front_hot_zone_, gate_position_in_front_, zone_thresh_)) {
                    objects_in_middle_earth_++;
                    middle_earth_update_time_ = what_time_is_it_now();
                }
            }

            front_zone_n = front_zone_n->next;
        }

        while (back_n) {
            auto *o = (Detector::object *) back_n->val;

            insertObject(back_temp_objects, back_zone_objects_, o->id, o->b.x, 1-o->z, o->vb.x, -o->vz,
                         back_hot_zone_, gate_position_in_back_);

            back_n = back_n->next;
        }

        back_zone_n = back_zone_objects_->front;
        while (back_zone_n) {
            auto *o = (GateCommander::object*) back_zone_n->val;

            if (o->in_zone_x_left && o->in_zone_x_right && o->in_zone_y_bot && o->in_zone_y_top) {
                if (isNearGate(o->x, o->y, back_hot_zone_, gate_position_in_back_, zone_thresh_)) {
                    objects_in_middle_earth_++;
                    middle_earth_update_time_ = what_time_is_it_now();
                }
            }

            back_zone_n = back_zone_n->next;
        }

        free_list_contents(front_zone_objects_);
        free_list_contents(back_zone_objects_);
        free_list(front_zone_objects_);
        free_list(back_zone_objects_);

        front_zone_objects_ = front_temp_objects;
        back_zone_objects_ = back_temp_objects;

        if ((what_time_is_it_now() - middle_earth_update_time_) > max_time_in_middle_earth_)
            objects_in_middle_earth_ = 0;

        if (objects_in_middle_earth_ > 0 || getNumberOfObjectsInFrontZone() > 0 || getNumberOfObjectsInBackZone() > 0) {
            command_ = true;
            filtered_command_ = command_median_filter_.filter(1.0);
        }
        else {
            command_ = false;
            filtered_command_ = command_median_filter_.filter(0.0);
        }
    }
}

void GateCommander::insertObject(list *l, list *objects, int id, float x, float y, float vx, float vy, box hot_zone, gate gate_pos) {
    float top_left_corner_x, top_left_corner_y;
    float bottom_right_corner_x, bottom_right_corner_y;
    GateCommander::object *obj = NULL;
    GateCommander::object obj_old;
    GateCommander::side gate_side, exit_side, entrance_side;
    bool in_zone_x, in_zone_y, is_first_time;

    top_left_corner_y = (float)(hot_zone.y - hot_zone.h / 2.0);
    if (top_left_corner_y < 0.0) top_left_corner_y = 0.0;

    top_left_corner_x = (float)(hot_zone.x - hot_zone.w / 2.0);
    if (top_left_corner_x < 0.0) top_left_corner_x = 0.0;

    bottom_right_corner_x = (float)(hot_zone.x + hot_zone.w / 2.0);
    if (bottom_right_corner_x > 1.0) bottom_right_corner_x = 1.0;

    bottom_right_corner_y = (float)(hot_zone.y + hot_zone.h / 2.0);
    if (bottom_right_corner_y > 1.0) bottom_right_corner_y = 1.0;

    obj = popObjectById(objects, id);

    if (!obj) {
        obj = (GateCommander::object*)malloc(sizeof(GateCommander::object));
        obj->id = id;
        obj->in_zone_x_left = false;
        obj->in_zone_x_right = false;
        obj->in_zone_y_top = false;
        obj->in_zone_y_bot = false;
        obj->x = x;
        obj->y = y;
        obj->vx = vx;
        obj->vy = vy;
        obj->going_towards_gate = false;
        is_first_time = true;
    }
    else is_first_time = false;

    obj_old.in_zone_y_bot = obj->in_zone_y_bot;
    obj_old.in_zone_y_top = obj->in_zone_y_top;
    obj_old.in_zone_x_left = obj->in_zone_x_left;
    obj_old.in_zone_x_right = obj->in_zone_x_right;
    obj_old.going_towards_gate = obj->going_towards_gate;
    obj_old.id = obj->id;
    obj_old.x = obj->x;
    obj_old.y = obj->y;
    obj_old.vx = obj->vx;
    obj_old.vy = obj->vy;

    in_zone_x = obj->in_zone_x_left && obj->in_zone_x_right;
    in_zone_y = obj->in_zone_y_bot && obj->in_zone_y_top;

    obj->in_zone_x_right = !hysteresis(!in_zone_x, x, hysteresis_T_, bottom_right_corner_x);
    obj->in_zone_x_left = hysteresis(in_zone_x, x, hysteresis_T_, top_left_corner_x);

    obj->in_zone_y_top = hysteresis(in_zone_y, y, hysteresis_T_, top_left_corner_y);
    obj->in_zone_y_bot = !hysteresis(!in_zone_y, y, hysteresis_T_, bottom_right_corner_y);

    obj->x = x;
    obj->y = y;
    obj->vx = vx;
    obj->vy = vy;

    gate_side = getSideFromDirection(gate_pos.direction, 1.0, 1.0);
    obj->going_towards_gate = isGoingTowardsGate(vx, vy, x, y, gate_pos, velocity_direction_thresh_, velocity_distance_thresh_);
    exit_side = getExitSide(obj, &obj_old);
    entrance_side = getEntranceSide(obj, &obj_old);

    if (is_first_time && obj->in_zone_x_right && obj->in_zone_x_left && obj->in_zone_y_top && obj->in_zone_y_bot) {
        if (isNearGate(obj->x, obj->y, hot_zone, gate_pos, zone_thresh_))
            objects_in_middle_earth_--;
    }

    if (gate_side != Unknown && gate_side == exit_side) {
        objects_in_middle_earth_++;
        middle_earth_update_time_ = what_time_is_it_now();
    }
    else if (gate_side != Unknown && gate_side == entrance_side) {
        objects_in_middle_earth_--;
        middle_earth_update_time_ = what_time_is_it_now();
    }
    if (objects_in_middle_earth_ < 0) objects_in_middle_earth_ = 0;

    list_insert(l, obj);
}

bool GateCommander::hysteresis(bool y, float x, float T, float center) {
    bool new_y = false;

    if (!y) {
        if (x >= center + T) new_y = true;
        else if (x < center + T) new_y = false;
    }
    else {
        if (x <= center - T) new_y = false;
        else if (x > center - T) new_y = true;
    }

    return new_y;
}

void GateCommander::setGateInFrontZone(float x, float y, float orientation, float w, float h) {
    is_gate_position_in_front_set_ = true;
    gate_position_in_front_.x = x;
    gate_position_in_front_.y = y;
    gate_position_in_front_.direction = orientation;
    gate_position_in_front_.height = h;
    gate_position_in_front_.width = w;
}

void GateCommander::setGateInBackZone(float x, float y, float orientation, float w, float h) {
    is_gate_position_in_back_set_ = true;
    gate_position_in_back_.x = x;
    gate_position_in_back_.y = y;
    gate_position_in_back_.direction = orientation;
    gate_position_in_back_.height = h;
    gate_position_in_back_.width = w;
}

void GateCommander::setHotZone(box front, box back) {
    is_hot_zone_initialized_ = true;
    front_hot_zone_ = front;
    back_hot_zone_ = back;
}

void GateCommander::setCheckMovementDirection(bool check_movement_direction) {
    check_speed_ = check_movement_direction;
}

void GateCommander::setMaxTimeInMiddleEarth(double max_time) {
        max_time_in_middle_earth_ = max_time;
}

void GateCommander::setCommandMedianFilterSize(int size) {
    command_median_filter_.init(size);
}

bool GateCommander::getCommand(void) {
    return command_;
}

bool GateCommander::getFilteredCommand(void) {
    if (filtered_command_ <= 0.5) return false;
    else return true;
}

double GateCommander::wrapTo2PI(double angle) {
    double wrapped_angle;

    wrapped_angle = fmod(angle, 2*M_PI);
    if (wrapped_angle < 0) wrapped_angle += 2*M_PI;

    return wrapped_angle;
}

bool GateCommander::isGoingTowardsGate(float vx, float vy, float x, float y, gate gate_pos, float velocity_direction_thresh, float position_thresh) {
    double dx, dy, angle;
    double angle_upper_limit, angle_lower_limit;
    double velocity_direction, magnitude_y, magnitude_x;
    bool is_going_towards_gate = false;

    dx = gate_pos.x - x;
    dy = gate_pos.y - y;

    angle = atan2(dy, dx);

    if (fabs(dy) > (gate_pos.height / 2.0))
        magnitude_y = fabs(dy) - (gate_pos.height / 2.0);
    else magnitude_y = 0.0;

    if (fabs(dx) > (gate_pos.width / 2.0))
        magnitude_x = fabs(dx) - (gate_pos.width / 2.0);
    else magnitude_x = 0.0;

    velocity_direction = atan2((double)vy, (double)vx);

    angle_lower_limit = angle - velocity_direction_thresh;
    angle_upper_limit = angle + velocity_direction_thresh;

    velocity_direction = wrapTo2PI(velocity_direction);
    angle_lower_limit = wrapTo2PI(angle_lower_limit);
    angle_upper_limit = wrapTo2PI(angle_upper_limit);

    if (magnitude_y < position_thresh && magnitude_x < position_thresh) { is_going_towards_gate = true; printf("Unutara saaam!\n");}
    else if (velocity_direction <= angle_upper_limit && velocity_direction >= angle_lower_limit) is_going_towards_gate = true;
    else if (angle_lower_limit > angle_upper_limit) {
        if (velocity_direction >= angle_lower_limit || velocity_direction <= angle_upper_limit) is_going_towards_gate = true;
        else is_going_towards_gate = false;
    }
    else is_going_towards_gate = false;

    return  is_going_towards_gate;
}

bool GateCommander::isNearGate(float x, float y, box hot_zone, gate gate_pos, float thresh) {
    GateCommander::side nearest_side = Unknown;
    GateCommander::side gate_side = Unknown;
    float top_left_corner_x, top_left_corner_y;
    float bottom_right_corner_x, bottom_right_corner_y;
    float dx, dy;
    float angle;

    top_left_corner_y = (float)(hot_zone.y - hot_zone.h / 2.0);
    if (top_left_corner_y < 0.0) top_left_corner_y = 0.0;

    top_left_corner_x = (float)(hot_zone.x - hot_zone.w / 2.0);
    if (top_left_corner_x < 0.0) top_left_corner_x = 0.0;

    bottom_right_corner_x = (float)(hot_zone.x + hot_zone.w / 2.0);
    if (bottom_right_corner_x > 1.0) bottom_right_corner_x = 1.0;

    bottom_right_corner_y = (float)(hot_zone.y + hot_zone.h / 2.0);
    if (bottom_right_corner_y > 1.0) bottom_right_corner_y = 1.0;

    dx = x - hot_zone.x;
    dy = y - hot_zone.y;

    angle = (float)atan2((double)dy, (double)dx);

    nearest_side = getSideFromDirection(angle, hot_zone.w, hot_zone.h);
    gate_side = getSideFromDirection(gate_pos.direction, 1.0, 1.0);

    if (nearest_side == gate_side && nearest_side != Unknown) {
        if (nearest_side == Top) {
            if ((y >= top_left_corner_y) && (y <= top_left_corner_y + thresh))
                return true;
        }
        else if (nearest_side == Bot) {
            if ((y <= bottom_right_corner_y) && (y >= bottom_right_corner_y - thresh))
                return true;
        }
        else if (nearest_side == Left) {
            if ((x >= top_left_corner_x) && (x <= top_left_corner_x + thresh))
                return true;
        }
        else if (nearest_side == Right) {
            if ((x <= bottom_right_corner_x) && (x >= bottom_right_corner_x - thresh))
                return true;
        }
    }

    return false;
}

GateCommander::side GateCommander::getExitSide(GateCommander::object *obj, GateCommander::object *obj_old) {
    GateCommander::side exit_side = Unknown;
    bool in_zone_x, in_zone_y, in_zone, in_zone_old;

    in_zone_x = obj->in_zone_x_right && obj->in_zone_x_left;
    in_zone_y = obj->in_zone_y_bot && obj->in_zone_y_top;

    in_zone = in_zone_x && in_zone_y;

    in_zone_x = obj_old->in_zone_x_right && obj_old->in_zone_x_left;
    in_zone_y = obj_old->in_zone_y_bot && obj_old->in_zone_y_top;

    in_zone_old = in_zone_x && in_zone_y;

    if (in_zone_old && !in_zone) {
        if (!obj->in_zone_x_right && obj->in_zone_x_left && obj->in_zone_y_bot && obj->in_zone_y_top) exit_side = Right;
        else if (obj->in_zone_x_right && !obj->in_zone_x_left && obj->in_zone_y_bot && obj->in_zone_y_top) exit_side = Left;
        else if (obj->in_zone_x_right && obj->in_zone_x_left && !obj->in_zone_y_bot && obj->in_zone_y_top) exit_side = Bot;
        else if (obj->in_zone_x_right && obj->in_zone_x_left && obj->in_zone_y_bot && !obj->in_zone_y_top) exit_side = Top;
        else exit_side = Unknown;
    }

    return exit_side;
}

GateCommander::side GateCommander::getEntranceSide(GateCommander::object *obj, GateCommander::object *obj_old) {
    GateCommander::side entrance_side = Unknown;
    bool in_zone_x, in_zone_y, in_zone, in_zone_old;

    in_zone_x = obj->in_zone_x_right && obj->in_zone_x_left;
    in_zone_y = obj->in_zone_y_bot && obj->in_zone_y_top;

    in_zone = in_zone_x && in_zone_y;

    in_zone_x = obj_old->in_zone_x_right && obj_old->in_zone_x_left;
    in_zone_y = obj_old->in_zone_y_bot && obj_old->in_zone_y_top;

    in_zone_old = in_zone_x && in_zone_y;

    if (!in_zone_old && in_zone) {
        if (!obj_old->in_zone_x_right && !obj_old->in_zone_x_left && !obj_old->in_zone_y_bot && !obj_old->in_zone_y_top) entrance_side = Unknown;
        else if (!obj_old->in_zone_x_right && obj_old->in_zone_x_left && obj_old->in_zone_y_bot && obj_old->in_zone_y_top) entrance_side = Right;
        else if (obj_old->in_zone_x_right && !obj_old->in_zone_x_left && obj_old->in_zone_y_bot && obj_old->in_zone_y_top) entrance_side = Left;
        else if (obj_old->in_zone_x_right && obj_old->in_zone_x_left && !obj_old->in_zone_y_bot && obj_old->in_zone_y_top) entrance_side = Bot;
        else if (obj_old->in_zone_x_right && obj_old->in_zone_x_left && obj_old->in_zone_y_bot && !obj_old->in_zone_y_top) entrance_side = Top;
        else entrance_side = Unknown;
    }

    return entrance_side;
}

GateCommander::side GateCommander::getSideFromDirection(float direction, float w, float h) {
    double dir, x, y;
    double angle_top_right, angle_top_left;
    double angle_bot_left, angle_bot_right;

    x = w / 2.0;
    y = h / 2.0;

    angle_top_right = atan2(-y, x);
    angle_top_right = wrapTo2PI(angle_top_right);

    angle_top_left = atan2(-y, -x);
    angle_top_left = wrapTo2PI(angle_top_left);

    angle_bot_left = atan2(y, -x);
    angle_bot_left = wrapTo2PI(angle_bot_left);

    angle_bot_right = atan2(y, x);
    angle_bot_right = wrapTo2PI(angle_bot_right);

    dir = wrapTo2PI(direction);

    if (((dir >= angle_top_right) && (dir <= 2*M_PI)) || ((dir >= 0.0) && (dir < angle_bot_right))) return Right;
    else if ((dir >= angle_bot_right) && (dir < angle_bot_left)) return Bot;
    else if ((dir >= angle_bot_left) && (dir < angle_top_left)) return Left;
    else if ((dir >= angle_top_left) && (dir < angle_top_right)) return Top;

    return Unknown;
}

int GateCommander::getNumberOfObjectsInFrontZone(void) {
    int number_of_objects = 0;
    node *n = front_zone_objects_->front;
    while(n) {
        auto *o = (GateCommander::object*) n->val;
        if (o->in_zone_x_left && o->in_zone_x_right && o->in_zone_y_bot && o->in_zone_y_top) {
            if (check_speed_)
            {
                if (o->going_towards_gate) number_of_objects++;
            }
            else number_of_objects++;
        }
        n = n->next;
    }
    return number_of_objects;
}

int GateCommander::getNumberOfObjectsInBackZone(void) {
    int number_of_objects = 0;
    node *n = back_zone_objects_->front;
    while(n) {
        auto *o = (GateCommander::object*) n->val;
        if (o->in_zone_x_left && o->in_zone_x_right && o->in_zone_y_bot && o->in_zone_y_top) {
            if (check_speed_)
            {
                if (o->going_towards_gate) number_of_objects++;
            }
            else number_of_objects++;
        }
        n = n->next;
    }
    return number_of_objects;
}

int GateCommander::getNumberOfObjectsInMiddleEarth(void) {
    return objects_in_middle_earth_;
}

GateCommander::object *GateCommander::popObjectById(list *l, int id) {
    node *n = l->front;
    node *n_prev, *n_next;

    while (n) {
        auto *p = (GateCommander::object*) n->val;
        if (p->id == id) {
            n_prev = n->prev;
            n_next = n->next;

            if (n_prev) n_prev->next = n_next;
            else l->front = n_next;

            if (n_next) n_next->prev = n_prev;
            else l->back = n_prev;

            --l->size;
            free(n);

            return p;
        }
        n = n->next;
    }
    return NULL;
}