//
// Created by suiauthon on 22.05.18..
//

#include <math.h>
#include <Clusterer.h>


Clusterer::Clusterer(void):
    max_delay_(0.35),
    min_time_(0.36),
    min_iou_(0.8),
    velocity_moving_average_filter_size_(60) {
    clusters_ = make_list();
}

Clusterer::~Clusterer(void) {
    freeClusters(clusters_);
}

void Clusterer::setMaxDelay(float max_delay) {
    max_delay_ = max_delay;
}

void Clusterer::setMinTime(float min_time) {
    min_time_ = min_time;
}

void Clusterer::setMinIou(float min_iou) {
    min_iou_ = min_iou;
}

void Clusterer::setVelocityMovingAverageFilterSize(int size) {
    velocity_moving_average_filter_size_ = size;
}

float Clusterer::estimateObjectZ(float x)
{
    float z;

    z = (1 - x);
    if (z<0) z = 0.0;

    return z;
}

void Clusterer::updateModel(double dt) {
    node *n_c = clusters_->front;
    node *n_o;
    Clusterer::classCluster *c;
    Clusterer::clusterObject *o;

    while(n_c) {
        c = (Clusterer::classCluster*) n_c->val;
        n_o = c->val->front;
        while(n_o) {
            o = (Clusterer::clusterObject*) n_o->val;
            if (fabs(o->filt->time_u - what_time_is_it_now()) > max_delay_) {
                removeNode(c->val, n_o);
            }
            else updateObjectFilterModel(o, dt);
            n_o = n_o->next;
        }
        n_c = n_c->next;
    }
}

list *Clusterer::getCluster(char *key) {
    Detector::object *o;
    list *l = make_list();
    list *c = findClassCluster(clusters_, key);

    if (c) {
        node *n = c->front;

        while (n) {
            auto *p = (Clusterer::clusterObject *) n->val;

            if (fabs(p->filt->time_s - p->filt->time_u) > min_time_) {
                o = (Detector::object*)malloc(sizeof(Detector::object));

                o->z = (float)p->filt->z.getPosition();
                o->vz = (float)p->filt->mz.filter((float)p->filt->z.getVelocity());
                o->prob = p->o.prob;
                o->id = p->o.id;
                o->b.x = (float)p->filt->x.getPosition();
                o->b.y = (float)p->filt->y.getPosition();
                o->b.w = (float)p->filt->w.getPosition();
                o->b.h = (float)p->filt->h.getPosition();
                o->vb.x = (float)p->filt->mx.filter((float)p->filt->x.getVelocity());
                o->vb.y = (float)p->filt->my.filter((float)p->filt->y.getVelocity());
                o->vb.w = (float)p->filt->mw.filter((float)p->filt->w.getVelocity());
                o->vb.h = (float)p->filt->mh.filter((float)p->filt->h.getVelocity());

                list_insert(l, o);
            }
            n = n->next;
        }
    }

    return l;
}

void Clusterer::updateMeasurement(Detector::objects *obj, double dt) {
    insertObjects(obj, dt);
}

void Clusterer::insertObjects(Detector::objects *obj, double dt) {
    Clusterer::clusterObject *o, *match_o;
    int i;

    if (obj->num) {
        o = (Clusterer::clusterObject*)malloc(sizeof(Clusterer::clusterObject));

        if (clusters_->size > 0) {
            for (i = 0; i < obj->num; i++) {
                o->o.prob = obj->prob[i];
                o->o.b = obj->b[i];
                o->o.z = estimateObjectZ(float(obj->b[i].y + obj->b[i].h / 2.0));
                match_o = findObject(clusters_, o, obj->name[i], dt);
                if (match_o == NULL) {
                    insertObject(clusters_, obj->name[i], o);
                }
                else {
                    match_o->o.b = o->o.b;
                    match_o->o.prob = o->o.prob;
                    match_o->o.z = o->o.z;
                    updateObjectFilterMeasurement(match_o);
                }
            }
        }
        else {
            for (i = 0; i < obj->num; i++) {
                o->o.prob = obj->prob[i];
                o->o.b = obj->b[i];
                o->o.z = estimateObjectZ(float(obj->b[i].y + obj->b[i].h / 2.0));
                insertObject(clusters_, obj->name[i], o);
            }
        }

        free(o);
    }
}

Clusterer::clusterObject *Clusterer::findObject(list *l, Clusterer::clusterObject *obj, char *key, double dt) {
    Clusterer::clusterObject *min_d_o = NULL;
    Clusterer::clusterObject *max_iou_o = NULL;
    list *class_cluster;
    float distance, iou, distance_tresh;

    class_cluster = findClassCluster(l, key);

    if (class_cluster != NULL) {
        min_d_o = findClosestObject(class_cluster, obj, &distance);
        max_iou_o = findMaxIouObject(class_cluster, obj, &iou);
        if (min_d_o == max_iou_o) {
            if (min_d_o != NULL) {
                distance_tresh = /*min_d_o->filt->y.getPosition();*/(float)(1-min_d_o->filt->z.getPosition());
                if (iou < min_iou_ && (distance/dt) > (2.0*distance_tresh)) {
                    min_d_o = NULL;
                }
            }
        }
        else {
            min_d_o = NULL;
        }
    }

    return min_d_o;
}

float Clusterer::calculateDistance(Clusterer::clusterObject *x, Clusterer::clusterObject *y) {
    float distance, dx, dy, dz;

    dx = (float)x->filt->x.getPosition()-y->o.b.x;
    dy = (float)x->filt->y.getPosition()-y->o.b.y;
    dz = (float)x->filt->z.getPosition()-y->o.z;

    distance = (float)sqrt(dx*dx + dy*dy + dz*dz);

    return distance;
}

Clusterer::clusterObject *Clusterer::findMaxIouObject(list *l, Clusterer::clusterObject *target, float *max_iou) {
    float iou;
    Clusterer::clusterObject *out = NULL, *temp_out;
    node *n = l->front;
    box b;

    *max_iou = -1;

    while(n) {
        if (n == l->front) {
            temp_out = (Clusterer::clusterObject*) n->val;
            if (!temp_out->filt->updated) {
                out = (Clusterer::clusterObject *) n->val;
                b.x = (float) out->filt->x.getPosition();
                b.y = (float) out->filt->y.getPosition();
                b.w = (float) out->filt->w.getPosition();
                b.h = (float) out->filt->h.getPosition();
                *max_iou = box_iou(b, target->o.b);
            }
        }
        else {
            temp_out = (Clusterer::clusterObject*) n->val;
            if (!temp_out->filt->updated) {
                b.x = (float) temp_out->filt->x.getPosition();
                b.y = (float) temp_out->filt->y.getPosition();
                b.w = (float) temp_out->filt->w.getPosition();
                b.h = (float) temp_out->filt->h.getPosition();
                iou = box_iou(b, target->o.b);

                if (iou > *max_iou) {
                    out = (Clusterer::clusterObject *) n->val;
                    *max_iou = iou;
                }
            }
        }
        n = n->next;
    }

    return out;
}

Clusterer::clusterObject *Clusterer::findClosestObject(list *l, Clusterer::clusterObject *target, float *dist) {
    float distance;
    Clusterer::clusterObject *out = NULL, *temp_out;
    node *n = l->front;

    *dist = 1;

    while(n) {
        if (n == l->front) {
            temp_out = (Clusterer::clusterObject*) n->val;
            if (!temp_out->filt->updated) {
                out = (Clusterer::clusterObject *) n->val;
                *dist = calculateDistance(out, target);
            }
        }
        else {
            temp_out = (Clusterer::clusterObject*) n->val;
            if (!temp_out->filt->updated) {
                distance = calculateDistance((Clusterer::clusterObject *) n->val, target);

                if (distance < *dist) {
                    out = (Clusterer::clusterObject *) n->val;
                    *dist = distance;
                }
            }
        }
        n = n->next;
    }

    return out;
}

void Clusterer::updateObjectFilterMeasurement(Clusterer::clusterObject *o)
{
    o->filt->time_u = what_time_is_it_now();
    o->filt->x.measureUpdate(o->o.b.x);
    o->filt->y.measureUpdate(o->o.b.y);
    o->filt->z.measureUpdate(o->o.z);
    o->filt->w.measureUpdate(o->o.b.w);
    o->filt->h.measureUpdate(o->o.b.h);
    o->filt->updated = true;
}

void Clusterer::initializeObjectFilter(Clusterer::clusterObject *o) {
    o->filt->time_s = what_time_is_it_now();
    o->filt->time_u = what_time_is_it_now();
    o->filt->x.initializePosition(o->o.b.x);
    o->filt->y.initializePosition(o->o.b.y);
    o->filt->z.initializePosition(o->o.z);
    o->filt->w.initializePosition(o->o.b.w);
    o->filt->h.initializePosition(o->o.b.h);
}

void Clusterer::updateObjectFilterModel(Clusterer::clusterObject *o, double dt)
{
    o->filt->x.modelUpdate(dt);
    o->filt->y.modelUpdate(dt);
    o->filt->z.modelUpdate(dt);
    o->filt->w.modelUpdate(dt);
    o->filt->h.modelUpdate(dt);
    o->filt->updated = false;
}

void Clusterer::setObjectId(list *l, Clusterer::clusterObject *obj) {
    Clusterer::clusterObject *temp_obj;
    node *n = NULL;
    int id = 0;
    bool found;

    while (true) {
        found = false;
        if (l) {
            n = l->front;
            while (n) {
                temp_obj = (Clusterer::clusterObject *) n->val;
                if (temp_obj->o.id == id) {
                    found = true;
                    break;
                }
                n = n->next;
            }
        }
        if (!found) {
            obj->o.id = id;
            break;
        }
        id++;
    }
    return;
}

void Clusterer::insertObject(list *l, char *key, Clusterer::clusterObject *val) {
    list *c = findClassCluster(l, key);
    Clusterer::clusterObject *new_val;

    new_val = (Clusterer::clusterObject*) malloc(sizeof(Clusterer::clusterObject));
    new_val->o.prob = val->o.prob;
    new_val->o.b = val->o.b;
    new_val->o.z = val->o.z;
    new_val->filt = new objectFilter();
    new_val->filt->updated = true;
    new_val->filt->mx.setSize(velocity_moving_average_filter_size_);
    new_val->filt->my.setSize(velocity_moving_average_filter_size_);
    new_val->filt->mz.setSize(velocity_moving_average_filter_size_);
    new_val->filt->mw.setSize(velocity_moving_average_filter_size_);
    new_val->filt->mh.setSize(velocity_moving_average_filter_size_);

    setObjectId(c, new_val);
    initializeObjectFilter(new_val);

    if (c == NULL) {
        Clusterer::classCluster *p = (Clusterer::classCluster*) malloc(sizeof(Clusterer::classCluster));
        p->key = key;
        p->val = make_list();
        list_insert(p->val, new_val);
        list_insert(l, p);
    }
    else {
        list_insert(c, new_val);
    }
}

void Clusterer::freeClusters(list *l) {
    node *n = l->front;
    node *n1;
    Clusterer::classCluster *p;
    Clusterer::clusterObject *o;
    while (n) {
        p = (Clusterer::classCluster*) n->val;
        n1 = p->val->front;
        while (n1) {
            o = (Clusterer::clusterObject*) n1->val;
            delete o->filt;
            n1 = n1->next;
        }
        free_list_contents(p->val);
        free_list(p->val);
        n = n->next;
    }
    free_list_contents(l);
    free_list(l);
}

void Clusterer::removeNode(list *l, node *n) {
    node *n_prev, *n_next;

    if (n) {
        free(n->val);
        n_prev = n->prev;
        n_next = n->next;

        if (n_prev) n_prev->next = n_next;
        else l->front = n_next;

        if (n_next) n_next->prev = n_prev;
        else l->back = n_prev;

        free(n);
        --l->size;
    }
}


list *Clusterer::findClassCluster(list *l, char *key) {
    node *n = l->front;
    while (n) {
        Clusterer::classCluster *p = (Clusterer::classCluster*) n->val;
        if (strcmp(p->key, key) == 0) {
            return p->val;
        }
        n = n->next;
    }
    return NULL;
}
