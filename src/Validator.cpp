#include <Validator.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

std::vector<std::string> split(std::string strToSplit, char delimeter)
{
    std::stringstream ss(strToSplit);
    std::string item;
    std::vector<std::string> splittedStrings;
    while (std::getline(ss, item, delimeter))
    {
        splittedStrings.push_back(item);
    }
    return splittedStrings;
}

void usage(char* progName)
{
    std::cout << progName << " DATACFG CFGFILE WEIGHTSFILE [OPTIONS]" << std::endl <<
    "Validates given neural network architecture with given weights." << std::endl <<
         "" << std::endl <<
         "    DATACFG:             Path to config file that containst names and number of classes." << std::endl <<
         "    CFGFILE:             Path to neural network architecture config file." << std::endl <<
         "    WEIGHTSFILE:         Path to weights list file." << std::endl <<
         "    OPTIONS:" << std::endl <<
         "      -i                 Use a specific GPU. (default: 0)" << std::endl <<
         "      -h | --help        Print a help message and exit." << std::endl <<
         "      -V | --Version     Display version information and exit." << std::endl;

}

void version(char* progName)
{
    std::cout << progName << " "<< VERSION_MAJOR << "." << VERSION_MINOR << " " << VERSION_DAY << VERSION_MONTH << VERSION_YEAR << std::endl;
}


int main(int argc, char **argv) {
    int gpu_i = -1;

    if (find_arg(argc, argv, (char*)"-h") || find_arg(argc, argv, (char*)"--help")) {
        usage(argv[0]);
        return 0;
    }
    else if (find_arg(argc, argv, (char*)"-V") || find_arg(argc, argv, (char*)"--version")) {
        version(argv[0]);
        return 0;
    }
    else if(argc < 2) {
        fprintf(stderr, "%s: missing  operand\n", argv[0]);
        fprintf(stderr, "Try '%s --help' or '%s -h' for more information.\n", argv[0], argv[0]);
        return 0;
    }
    else if (argc == 2){
        fprintf(stderr, "%s: wrong usage\n", argv[0]);
        fprintf(stderr, "Try '%s --help' or '%s -h' for more information.\n", argv[0], argv[0]);
        return 0;
    }
    else {
        std::string temp1(argv[1]);
        std::string temp2(argv[2]);
        std::string temp3(argv[3]);

        if (temp1.find(std::string(".data")) == std::string::npos || temp2.find(std::string(".cfg")) == std::string::npos ||
                temp3.find(std::string(".txt")) == std::string::npos) {
            fprintf(stderr, "%s: wrong usage\n", argv[0]);
            fprintf(stderr, "Try '%s --help' or '%s -h' for more information.\n", argv[0], argv[0]);
            return 0;
        }
    }

    gpu_i = find_int_arg(argc, argv, (char*)"-i", 0);
    if(find_arg(argc, argv, (char*)"-nogpu")) {
        gpu_i = -1;
    }

#ifndef GPU
    gpu_i = -1;
#endif

    Detector d(argv[1]);
    d.loadNetwork(argv[2], NULL, gpu_i);

    std::ifstream input(argv[3]);

    for( std::string line; getline( input, line ); ) {
        char * line1 = const_cast<char*> ( line.c_str() );
        printf("Loading weights %s\n", line1);
        d.loadWeights(line1);
        std::vector<std::string> name;
        char del = '/';
        name = split(line,del);
        std::vector<std::string> name1;
        del = '.';
        name1 = split(name[name.size()-1], del);
        char * name2 = const_cast<char*> ( name1[0].c_str() );
        d.validate(name2, 0.25);
    }

    return 0;
}