#include <Detector.h>

#define BLUE_R (float)0.0
#define BLUE_G (float)0.4470
#define BLUE_B (float)0.7410
#define ORANGE_R (float)0.8500
#define ORANGE_G (float)0.3250
#define ORANGE_B (float)0.0980
#define YELLOW_R (float)0.9290
#define YELLOW_G (float)0.6940
#define YELLOW_B (float)0.1250
#define PURPLE_R (float)0.4940
#define PURPLE_G (float)0.1840
#define PURPLE_B (float)0.5560
#define GREEN_R (float)0.4660
#define GREEN_G (float)0.6740
#define GREEN_B (float)0.1880
#define TURQUOISE_R (float)0.3010
#define TURQUOISE_G (float)0.7450
#define TURQUOISE_B (float)0.9330
#define RED_R (float)0.6350
#define RED_G (float)0.0780
#define RED_B (float)0.1840


Detector::Detector(char *datacfg):
        is_prediction_filter_initialized_(0),
        actual_prediction_filter_size_(1),
        prediction_filter_index_(0),
        prediction_filter_size_(1),
        is_network_loaded_(0),
        net_size_(0),
        confidence_threshold_(.3),
        hier_thresh_(.25),
        nms_(.45) {

    options_ = read_data_cfg(datacfg);

    classes_ = option_find_int(options_, (char*)"classes", 20);
    name_list_ = option_find_str(options_, (char*)"names", (char*)"data/names.list");
    train_images_ = option_find_str(options_, (char*)"train", (char*)"data/train.list");
    valid_images_ = option_find_str(options_, (char*)"valid", (char*)"data/train.list");
    backup_directory_ = option_find_str(options_, (char*)"backup", (char*)"/backup/");
    results_directory_ = option_find_str(options_, (char*)"results", (char*)"results");
    alphabet_ = load_alphabet();
    names_ = get_labels(name_list_);
}

Detector::~Detector(void) {
    int j, i;

    if (is_network_loaded_) {
        if (is_prediction_filter_initialized_) {
            for (j = 0; j < prediction_filter_size_; ++j) free(predictions_[j]);
            free(predictions_);
            free(avg_);
        }

        free_network(net_);
    }

    for (i=0; i<8; i++) {
        for (j=0;j<128;j++) {
            free_image(alphabet_[i][j]);
        }
        free(alphabet_[i]);
    }
    free(alphabet_);

    free(names_);
    free_list_contents(options_);
    free_list(options_);
}

int Detector::loadNetwork(char *cfgfile, char *weightfile, int gpu_i) {
    if (!is_network_loaded_) {
        is_network_loaded_ = 1;

        gpu_index = gpu_i;

        if (gpu_index >= 0) {
            printf("gpu index %d\n", gpu_index);
            cuda_set_device(gpu_index);
        }

        net_ = load_network(cfgfile, weightfile, 0);
        set_batch_network(net_, 1);
        srand(time(NULL));

        net_size_ = networkSize(net_);
    }

    return is_network_loaded_;
}

void Detector::loadWeights(char *weightfile) {
    if (is_network_loaded_) {
        if(weightfile && weightfile[0] != 0){
            load_weights(net_, weightfile);
        }
    }
}

void Detector::setConfidenceThreshold(float thresh) {
    confidence_threshold_ = thresh;
}

void Detector::setHierThresh(float hier_thresh) {
    hier_thresh_ = hier_thresh;
}

void Detector::setNms(float nms) {
    nms_ = nms;
}

int Detector::networkSize(network *net) {
    int i;
    int count = 0;
    for(i = 0; i < net->n; ++i){
        layer l = net->layers[i];
        if(l.type == YOLO || l.type == REGION || l.type == DETECTION){
            count += l.outputs;
        }
    }
    return count;
}

int Detector::initializePredictionFilter(int filter_size) {
    int j;

    if (is_network_loaded_) {
        prediction_filter_size_ = filter_size;
        prediction_filter_index_ = 0;

        predictions_ = (float **) calloc(prediction_filter_size_, sizeof(float*));
        avg_ = (float *) calloc(net_size_, sizeof(float));
        for(j = 0; j < prediction_filter_size_; ++j) predictions_[j] = (float *) calloc(net_size_, sizeof(float));
        is_prediction_filter_initialized_ = 1;
    }

    return is_prediction_filter_initialized_;
}

Detector::objects *Detector::getDetectedObjects(detection *dets, int num, float thresh, char **names, int classes) {
    int i, j, index, objects_number = 0;
    objects *detected_objects = (objects*) calloc(1, sizeof(objects));

    for (i = 0; i < num; ++i) {
        index = -1;
        for (j = 0; j < classes; ++j) {
            if (dets[i].prob[j] > thresh) {
                if (index < 0) index = j;
                else if (dets[i].prob[j] > dets[i].prob[index]) index = j;
            }
        }
        if (index >= 0) {
            objects_number++;
            if (objects_number == 1) {
                detected_objects->obj = (object*) calloc(objects_number, sizeof(object));
                detected_objects->prob = (float*) calloc(objects_number, sizeof(float));
                detected_objects->b = (box*) calloc(objects_number, sizeof(box));
                detected_objects->name = (char**) calloc(objects_number, sizeof(char*));
            }
            else {
                detected_objects->obj = (object*) realloc(detected_objects->obj, objects_number*sizeof(object));
                detected_objects->prob = (float*) realloc(detected_objects->prob, objects_number*sizeof(float));
                detected_objects->b = (box*) realloc(detected_objects->b, objects_number*sizeof(box));
                detected_objects->name = (char**) realloc(detected_objects->name, objects_number*sizeof(char*));
            }
            detected_objects->obj[objects_number-1].b = dets[i].bbox;
            detected_objects->obj[objects_number-1].prob = dets[i].prob[index];
            detected_objects->prob[objects_number-1] = dets[i].prob[index];
            detected_objects->b[objects_number-1] = dets[i].bbox;
            detected_objects->name[objects_number-1] = names[index];
        }
    }
    detected_objects->num = objects_number;
    return detected_objects;
}

void Detector::rememberNetwork(network *net, int index, float **predictions) {
    int i;
    int count = 0;
    for(i = 0; i < net->n; ++i){
        layer l = net->layers[i];
        if(l.type == YOLO || l.type == REGION || l.type == DETECTION){
            memcpy(predictions[index] + count, net->layers[i].output, sizeof(float) * l.outputs);
            count += l.outputs;
        }
    }
}

void Detector::avgPredictions(network *net, int net_size, float *avg, float **predictions, int filter_size) {
    int i, j;
    int count = 0;
    fill_cpu(net_size, 0, avg, 1);
    for(j = 0; j < filter_size; ++j){
        axpy_cpu(net_size, 1./filter_size, predictions[j], 1, avg, 1);
    }
    for(i = 0; i < net->n; ++i){
        layer l = net->layers[i];
        if(l.type == YOLO || l.type == REGION || l.type == DETECTION){
            memcpy(l.output, avg + count, sizeof(float) * l.outputs);
            count += l.outputs;
        }
    }
}

void Detector::freeObjects(objects *obj) {
    free(obj->b);
    free(obj->prob);
    free(obj->name);
    free(obj->obj);
    free(obj);
}

Detector::objects *Detector::copyObjects(objects *obj) {
    Detector::objects *o = (objects*) calloc(1, sizeof(objects));;
    int i;

    o->num = obj->num;
    o->obj = (object*) calloc(obj->num, sizeof(object));
    o->prob = (float*) calloc(obj->num, sizeof(float));
    o->b = (box*) calloc(obj->num, sizeof(box));
    o->name = (char**) calloc(obj->num, sizeof(char*));

    for (i = 0; i < obj->num; i++) {
        o->obj[i].b = obj->obj[i].b;
        o->obj[i].prob = obj->obj[i].prob;
        o->prob[i] = obj->prob[i];
        o->b[i] = obj->b[i];
        o->name[i] = obj->name[i];
    }

    return o;
}

int Detector::detectionsComparator(const void *pa, const void *pb)
{
    box_prob a = *(box_prob *)pa;
    box_prob b = *(box_prob *)pb;
    float diff = a.p - b.p;
    if (diff < 0) return 1;
    else if (diff > 0) return -1;
    return 0;
}

void Detector::freeNetwork(void) {
    if (is_network_loaded_) free_network(net_);
    is_network_loaded_ = 0;
}

void Detector::validate(char *outfile, float thresh_calc_avg_iou) {
    list *plist;
    layer l;
    pthread_t *thr;
    load_args args = {0};
    box_prob *detections;
    detection *dets;
    FILE *fp = 0;
    image *val, *val_resized, *buf, *buf_resized;
    char **paths, *path, buff[1024], *id;
    int *truth_classes_count, image_index;
    int classes, j, m, t, nthreads, i = 0, it = 0;
    int fp_for_thresh = 0, tp_for_thresh = 0;
    int detections_count = 0, unique_truth_count = 0;
    int nboxes = 0, w, h, *map = 0, num_labels;
    int checkpoint_detections_count, class_id;
    int truth_index;
    float thresh, nms, iou_thresh, avg_iou = 0, *X;
    float prob, max_iou;
    double start_time;

    srand(time(NULL));

    plist = get_paths(valid_images_);
    paths = (char **) list_to_array(plist);

    l = net_->layers[net_->n - 1];
    classes = l.classes;


    snprintf(buff, 1024, "%s/%s.txt", results_directory_, outfile);
    fp = fopen(buff, "w");

    m = plist->size;

    thresh = .005;
    nms = .45;
    iou_thresh = 0.5;

    nthreads = 4;
    val = (image *) calloc(nthreads, sizeof(image));
    val_resized = (image *) calloc(nthreads, sizeof(image));
    buf = (image *) calloc(nthreads, sizeof(image));
    buf_resized = (image *) calloc(nthreads, sizeof(image));
    thr = (pthread_t *) calloc(nthreads, sizeof(pthread_t));

    args.w = net_->w;
    args.h = net_->h;
    args.type = LETTERBOX_DATA;

    detections = (box_prob *) calloc(1, sizeof(box_prob));
    truth_classes_count = (int *) calloc(classes, sizeof(int));

    for (t = 0; t < nthreads; ++t) {
        args.path = paths[i + t];
        args.im = &buf[t];
        args.resized = &buf_resized[t];
        thr[t] = load_data_in_thread(args);
    }
    start_time = what_time_is_it_now();
    for (i = nthreads; i < m + nthreads; i += nthreads) {
        fprintf(stderr, "%d\n", i);
        for (t = 0; t < nthreads && i + t - nthreads < m; ++t) {
            pthread_join(thr[t], 0);
            val[t] = buf[t];
            val_resized[t] = buf_resized[t];
        }
        for (t = 0; t < nthreads && i + t < m; ++t) {
            args.path = paths[i + t];
            args.im = &buf[t];
            args.resized = &buf_resized[t];
            thr[t] = load_data_in_thread(args);
        }
        for (t = 0; t < nthreads && i + t - nthreads < m; ++t) {
            image_index = i + t - nthreads;
            path = paths[image_index];
            id = basecfg(path);
            X = val_resized[t].data;
            network_predict(net_, X);

            nboxes = 0;
            w = val[t].w;
            h = val[t].h;
            dets = get_network_boxes(net_, w, h, thresh, .5, map, 0, &nboxes); //w i h iz nekog razloga nula stavi

            if (nms) do_nms_sort(dets, nboxes, classes, nms);

            char labelpath[4096];
            find_replace(path, (char*)"images", (char*)"labels", labelpath);
            find_replace(labelpath, (char*)"JPEGImages", (char*)"labels", labelpath);
            find_replace(labelpath, (char*)".jpg", (char*)".txt", labelpath);
            find_replace(labelpath, (char*)".JPG", (char*)".txt", labelpath);
            find_replace(labelpath, (char*)".JPEG", (char*)".txt", labelpath);
            num_labels = 0;
            box_label *truth = read_boxes(labelpath, &num_labels);

            for (j = 0; j < num_labels; ++j) {
                truth_classes_count[truth[j].id]++;
            }

            checkpoint_detections_count = detections_count;

            for (it = 0; it < nboxes; ++it) {
                for (class_id = 0; class_id < classes; ++class_id) {
                    prob = dets[it].prob[class_id];
                    if (prob > 0) {
                        detections_count++;
                        detections = (box_prob *) realloc(detections, detections_count * sizeof(box_prob));
                        detections[detections_count - 1].b = dets[it].bbox;
                        detections[detections_count - 1].p = prob;
                        detections[detections_count - 1].image_index = image_index;
                        detections[detections_count - 1].class_id = class_id;
                        detections[detections_count - 1].truth_flag = 0;
                        detections[detections_count - 1].unique_truth_index = -1;

                        truth_index = -1;
                        max_iou = 0;

                        for (j = 0; j < num_labels; ++j) {
                            box boxt = {truth[j].x*w, truth[j].y*h, truth[j].w*w, truth[j].h*h};
                            float current_iou = box_iou(dets[it].bbox, boxt);
                            if (current_iou > iou_thresh && class_id == truth[j].id) {
                                if (current_iou > max_iou) {
                                    max_iou = current_iou;
                                    truth_index = unique_truth_count + j;
                                }
                            }
                        }

                        if (truth_index > -1) {
                            detections[detections_count - 1].truth_flag = 1;
                            detections[detections_count - 1].unique_truth_index = truth_index;
                        }

                        // calc avg IoU, true-positives, false-positives for required Threshold
                        if (prob > thresh_calc_avg_iou) {
                            int z, found = 0;
                            for (z = checkpoint_detections_count; z < detections_count - 1; ++z)
                                if (detections[z].unique_truth_index == truth_index) {
                                    found = 1;
                                    break;
                                }

                            if (truth_index > -1 && found == 0) {
                                avg_iou += max_iou;
                                ++tp_for_thresh;
                            } else
                                fp_for_thresh++;
                        }
                    }
                }
            }

            unique_truth_count += num_labels;

            free_detections(dets, nboxes);
            free(id);
            free_image(val[t]);
            free_image(val_resized[t]);
        }
    }

    if ((tp_for_thresh + fp_for_thresh) > 0)
        avg_iou = avg_iou / (tp_for_thresh + fp_for_thresh);

    // SORT(detections)
    qsort(detections, detections_count, sizeof(box_prob), &Detector::detectionsComparator);

    // for PR-curve
    pr_t **pr = (pr_t **) calloc(classes, sizeof(pr_t *));
    for (i = 0; i < classes; ++i) {
        pr[i] = (pr_t *) calloc(detections_count, sizeof(pr_t));
    }
    fprintf(fp, "detections_count = %d, unique_truth_count = %d  \n", detections_count, unique_truth_count);

    int *truth_flags = (int *) calloc(unique_truth_count, sizeof(int));

    int rank;
    for (rank = 0; rank < detections_count; ++rank) {
        if (rank % 100 == 0)
            fprintf(fp, " rank = %d of ranks = %d \r", rank, detections_count);

        if (rank > 0) {
            int class_id;
            for (class_id = 0; class_id < classes; ++class_id) {
                pr[class_id][rank].tp = pr[class_id][rank - 1].tp;
                pr[class_id][rank].fp = pr[class_id][rank - 1].fp;
            }
        }

        box_prob d = detections[rank];
        // if (detected && isn't detected before)
        if (d.truth_flag == 1) {
            if (truth_flags[d.unique_truth_index] == 0) {
                truth_flags[d.unique_truth_index] = 1;
                pr[d.class_id][rank].tp++;    // true-positive
            }
        } else {
            pr[d.class_id][rank].fp++;    // false-positive
        }

        for (i = 0; i < classes; ++i) {
            const int tp = pr[i][rank].tp;
            const int fp = pr[i][rank].fp;
            const int fn = truth_classes_count[i] - tp;    // false-negative = objects - true-positive
            pr[i][rank].fn = fn;

            if ((tp + fp) > 0) pr[i][rank].precision = (double) tp / (double) (tp + fp);
            else pr[i][rank].precision = 0;

            if ((tp + fn) > 0) pr[i][rank].recall = (double) tp / (double) (tp + fn);
            else pr[i][rank].recall = 0;
        }
    }

    free(truth_flags);

    double mean_average_precision = 0;

    for (i = 0; i < classes; ++i) {
        double avg_precision = 0;
        int point;
        for (point = 0; point < 11; ++point) {
            double cur_recall = point * 0.1;
            double cur_precision = 0;
            for (rank = 0; rank < detections_count; ++rank) {
                if (pr[i][rank].recall >= cur_recall) {    // > or >=
                    if (pr[i][rank].precision > cur_precision) {
                        cur_precision = pr[i][rank].precision;
                    }
                }
            }
            //printf("class_id = %d, point = %d, cur_recall = %.4f, cur_precision = %.4f \n", i, point, cur_recall, cur_precision);

            avg_precision += cur_precision;
        }
        avg_precision = avg_precision / 11;
        fprintf(fp, "class_id = %d, name = %s, \t ap = %2.2f %% \n", i, names_[i], avg_precision * 100);
        mean_average_precision += avg_precision;
    }

    const float cur_precision = (float) tp_for_thresh / ((float) tp_for_thresh + (float) fp_for_thresh);
    const float cur_recall =
            (float) tp_for_thresh / ((float) tp_for_thresh + (float) (unique_truth_count - tp_for_thresh));
    const float f1_score = 2.F * cur_precision * cur_recall / (cur_precision + cur_recall);
    fprintf(fp, "for thresh = %1.2f, precision = %1.2f, recall = %1.2f, F1-score = %1.2f \n",
            thresh_calc_avg_iou, cur_precision, cur_recall, f1_score);

    fprintf(fp, "for thresh = %0.2f, TP = %d, FP = %d, FN = %d, average IoU = %2.2f %% \n",
            thresh_calc_avg_iou, tp_for_thresh, fp_for_thresh, unique_truth_count - tp_for_thresh, avg_iou * 100);

    mean_average_precision = mean_average_precision / classes;
    fprintf(fp, "\nmean average precision (mAP) = %f, or %2.2f %% \n", mean_average_precision,
            mean_average_precision * 100);

    for (i = 0; i < classes; ++i) {
        free(pr[i]);
    }
    free(pr);
    free(detections);
    free(truth_classes_count);

    fclose(fp);

    fprintf(stderr, "Total Detection Time: %f Seconds\n", what_time_is_it_now() - start_time);
}

void Detector::train(char *cfgfile, char *weightfile, int *gpus, int ngpus, int clear) {
    int seed, i, imgs, classes, count = 0;
    char *base, **paths;
    float avg_loss = -1, jitter;
    double current_time;
    network **nets, *net;
    data train, buffer;
    load_args args;
    layer l;
    list *plist;
    pthread_t load_thread;

    base = basecfg(cfgfile);
    nets = (network**)calloc(ngpus, sizeof(network));

    srand(time(NULL));
    seed = rand();
    for(i = 0; i < ngpus; ++i){
        srand(seed);
#ifdef GPU
        cuda_set_device(gpus[i]);
#endif
        nets[i] = load_network(cfgfile, weightfile, clear);
        nets[i]->learning_rate *= ngpus;
    }
    srand(time(NULL));
    net = nets[0];

    imgs = net->batch * net->subdivisions * ngpus;
    printf("Learning Rate: %g, Momentum: %g, Decay: %g\n", net->learning_rate, net->momentum, net->decay);

    l = net->layers[net->n - 1];

    classes = l.classes;
    jitter = l.jitter;

    plist = get_paths(train_images_);
    paths = (char **)list_to_array(plist);

    args = get_base_args(net);
    args.coords = l.coords;
    args.paths = paths;
    args.n = imgs;
    args.m = plist->size;
    args.classes = classes;
    args.jitter = jitter;
    args.num_boxes = l.max_boxes;
    args.d = &buffer;
    args.type = DETECTION_DATA;
    args.threads = 64;

    load_thread = load_data(args);

    while(get_current_batch(net) < (unsigned)net->max_batches){
        if(l.random && count++%10 == 0){
            printf("Resizing\n");
            int dim = (rand() % 10 + 10) * 32;
            if (get_current_batch(net)+200 > (unsigned)net->max_batches) dim = 608;
            printf("%d\n", dim);
            args.w = dim;
            args.h = dim;

            pthread_join(load_thread, 0);
            train = buffer;
            free_data(train);
            load_thread = load_data(args);

            #pragma omp parallel for
            for(i = 0; i < ngpus; ++i){
                resize_network(nets[i], dim, dim);
            }
            net = nets[0];
        }
        current_time=what_time_is_it_now();
        pthread_join(load_thread, 0);
        train = buffer;
        load_thread = load_data(args);

        printf("Loaded: %lf seconds\n", what_time_is_it_now()-current_time);

        current_time=what_time_is_it_now();
        float loss = 0;
#ifdef GPU
        if(ngpus == 1){
            loss = train_network(net, train);
        } else {
            loss = train_networks(nets, ngpus, train, 4);
        }
#else
        loss = train_network(net, train);
#endif
        if (avg_loss < 0) avg_loss = loss;
        avg_loss = avg_loss*.9 + loss*.1;

        i = get_current_batch(net);
        printf("%ld: %f, %f avg, %f rate, %lf seconds, %d images\n", get_current_batch(net), loss, avg_loss, get_current_rate(net), what_time_is_it_now()-current_time, i*imgs);
        if(i%100==0){
#ifdef GPU
            if(ngpus != 1) sync_nets(nets, ngpus, 0);
#endif
            char buff[256];
            sprintf(buff, "%s/%s.backup", backup_directory_, base);
            save_weights(net, buff);
        }
        if(i%1000==0 || (i < 1000 && i%100 == 0)){
#ifdef GPU
            if(ngpus != 1) sync_nets(nets, ngpus, 0);
#endif
            char buff[256];
            sprintf(buff, "%s/%s_%d.weights", backup_directory_, base, i);
            save_weights(net, buff);
        }
        free_data(train);
    }
#ifdef GPU
    if(ngpus != 1) sync_nets(nets, ngpus, 0);
#endif
    char buff[256];
    sprintf(buff, "%s/%s_final.weights", backup_directory_, base);
    save_weights(net, buff);

    for(i = 0; i < ngpus; ++i) {
        free_network(nets[i]);
    }
    free_ptrs((void**)paths, plist->size);
    free_list(plist);
    free(base);
}

void Detector::drawDetection(image im, box b, char *name, Detector::colors color, float thickness, float font_size) {
    int left, right, top, bot;
    int width = (int)(im.h * thickness);
    float rgb[3];

    if (color == Blue) {
        rgb[0] = BLUE_R;
        rgb[1] = BLUE_G;
        rgb[2] = BLUE_B;
    }
    else if (color == Orange) {
        rgb[0] = ORANGE_R;
        rgb[1] = ORANGE_G;
        rgb[2] = ORANGE_B;
    }
    else if (color == Yellow) {
        rgb[0] = YELLOW_R;
        rgb[1] = YELLOW_G;
        rgb[2] = YELLOW_B;
    }
    else if (color == Purple) {
        rgb[0] = PURPLE_R;
        rgb[1] = PURPLE_G;
        rgb[2] = PURPLE_B;
    }
    else if (color == Green) {
        rgb[0] = GREEN_R;
        rgb[1] = GREEN_G;
        rgb[2] = GREEN_B;
    }
    else if (color == Turquoise) {
        rgb[0] = TURQUOISE_R;
        rgb[1] = TURQUOISE_G;
        rgb[2] = TURQUOISE_B;
    }
    else if (color == Red) {
        rgb[0] = RED_R;
        rgb[1] = RED_G;
        rgb[2] = RED_B;
    }
    else return;

    left  = (int)((b.x-b.w/2.)*im.w);
    right = (int)((b.x+b.w/2.)*im.w);
    top   = (int)((b.y-b.h/2.)*im.h);
    bot   = (int)((b.y+b.h/2.)*im.h);

    if(left < 0) left = 0;
    if(right > im.w-1) right = im.w-1;
    if(top < 0) top = 0;
    if(bot > im.h-1) bot = im.h-1;

    draw_box_width(im, left, top, right, bot, width, rgb[0], rgb[1], rgb[2]);

    if (alphabet_) {
        image label = get_label(alphabet_, name, (int)(im.h*font_size));
        draw_label(im, top + width, left, label, rgb);
        free_image(label);
    }

}

int Detector::getNetworkWidth(void) {
    return net_->w;
}

int Detector::getNetworkHeight(void) {
    return net_->h;
}

Detector::objects *Detector::predict(image im) {
    image sized;
    int nboxes = 0;
    float *X;
    detection *dets;
    objects *detected_objects = NULL;
    layer l;

    if (is_network_loaded_) {
        sized = letterbox_image(im, net_->w, net_->h);
        l = net_->layers[net_->n-1];
        X = sized.data;

        network_predict(net_, X);

        if (is_prediction_filter_initialized_) {
            rememberNetwork(net_, prediction_filter_index_, predictions_);
            avgPredictions(net_, net_size_, avg_, predictions_, actual_prediction_filter_size_);
            prediction_filter_index_ = (prediction_filter_index_ + 1)%prediction_filter_size_;
            if (actual_prediction_filter_size_ < prediction_filter_size_) {
                actual_prediction_filter_size_ += 1;
            }
        }

        dets = get_network_boxes(net_, im.w, im.h, confidence_threshold_, hier_thresh_, 0, 1, &nboxes);

        if (nms_) do_nms_sort(dets, nboxes, l.classes, nms_);

        detected_objects = getDetectedObjects(dets, nboxes, confidence_threshold_, names_, l.classes);

        free_image(sized);
        free_detections(dets, nboxes);
    }

    return detected_objects;
}
