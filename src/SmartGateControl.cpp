#include <SmartGateControl.h>
#include <mutex>
#include <unistd.h>

void matIntoImage(cv::Mat* src, image im)
{
    unsigned char *data = (unsigned char *)src->data;
    int h = src->rows;
    int w = src->cols;
    int c = src->channels();
    int i, j, k, color_index = 0, index;

    for (k = 0; k < c; k++) {
        color_index = (c - k + 2) % c;
        for (j = 0; j < h; j++) {
            for (i = 0; i < w; i++) {
                index = i + w*j + w*h*k;
                im.data[index] = (float)data[color_index + (i + w*j) * c] / 255.0;
            }
        }
    }
}

void imageIntoMat(image im, cv::Mat* src)
{
    unsigned char *data = (unsigned char *)im.data;
    src->rows = im.h;
    src->cols = im.w;
    int h = src->rows;
    int w = src->cols;
    int c = src->channels();
    int i, j, k, color_index = 0, index;
    for (k = 0; k < c; k++) {
        color_index = (c - k + 2) % c;
        for (j = 0; j < h; j++) {
            for (i = 0; i < w; i++) {
                index = i + w*j + w*h*k;
                src->data[color_index + (i + w*j) * c] = im.data[index] * 255;
            }
        }
    }
}

class SharedImage {
private:
    image im_;
    std::mutex m_mutex_;
    double timestamp_;

public:
    bool first_pass_;

    SharedImage(void) {
        first_pass_ = true;
        timestamp_ = 0.0;
    };

    ~SharedImage(void) {
        free_image(im_);
    };

    void setImage(image p) {
        std::lock_guard<std::mutex> guard(m_mutex_);
        if (first_pass_) {
            im_ = p;
            im_.data = (float*)calloc(p.h * p.w * p.c, sizeof(float));
            first_pass_ = false;
        }
        memcpy(im_.data, p.data, p.h*p.w*p.c*sizeof(float));
        timestamp_ = what_time_is_it_now();
    };

    image getImage(void) {
        std::lock_guard<std::mutex> guard(m_mutex_);
        return copy_image(im_);
    };

    double getTimestamp(void) {
        std::lock_guard<std::mutex> guard(m_mutex_);
        return timestamp_;
    };
};

class SharedObjects {
private:
    Detector::objects *obj_;
    std::mutex m_mutex_;
    double timestamp_;

public:

    SharedObjects(void) {
        obj_ = NULL;
        timestamp_ = 0.0;
    };

    ~SharedObjects(void) {
        if (obj_) Detector::freeObjects(obj_);
    };

    void setObjects(Detector::objects *o) {
        std::lock_guard<std::mutex> guard(m_mutex_);
        if (obj_) Detector::freeObjects(obj_);
        obj_ = Detector::copyObjects(o);
        timestamp_ = what_time_is_it_now();
    };

    Detector::objects* getObjects(void) {
        std::lock_guard<std::mutex> guard(m_mutex_);
        return Detector::copyObjects(obj_);
    };

    double getTimestamp(void) {
        std::lock_guard<std::mutex> guard(m_mutex_);
        return timestamp_;
    };
};

typedef struct detectThreadData {
    SharedImage *image;
    SharedObjects *obj;
    Detector *detector;
    volatile bool is_running;
} detectThreadData;

typedef struct grabThreadData {
    char*  video;
    Grabber *grabber;
    SharedImage *image;
    volatile bool is_running;
} grabThreadData;

typedef struct processThreadData {
    GateCommander *persons_gate_commander;
    GateCommander *forklifts_gate_commander;
    Clusterer *front_cam_image_clusterer;
    Clusterer *back_cam_image_clusterer;
    SharedObjects *front_cam_obj;
    SharedObjects *back_cam_obj;
    volatile bool is_running;
} processThreadData;

void *grab_in_thread(void *ptr) {
    grabThreadData *data = (grabThreadData*)ptr;
    double time_beginning;
    cv::Mat frame;
    int rate;
    image im;

    //im = load_image_color(data->video,0,0);

    cv::VideoCapture cap(data->video);
    if(!cap.isOpened()) error("Couldn't open video file.\n");

    cap >> frame;

    im = make_image(frame.cols, frame.rows, frame.channels());

    data->is_running = true;

    while(data->is_running) {
        time_beginning = what_time_is_it_now();

        cap >> frame;
        if(frame.empty()) break;
        matIntoImage(&frame, im);
        (data->image)->setImage(im);

        rate = (int)((GRAB_RATE - (what_time_is_it_now()-time_beginning))*1000000);
        if (rate < 0) rate = 0;
        usleep(rate);
    }

    free_image(im);

    data->is_running = false;

    pthread_exit(NULL);
}

void *detect_in_thread(void *ptr) {
    detectThreadData *data = (detectThreadData*)ptr;
    Detector::objects *obj;

    double time_beginning, dt;
    int rate;
    image im;

    data->is_running = true;

    while(data->is_running) {
        time_beginning = what_time_is_it_now();

        dt = what_time_is_it_now() - (data->image)->getTimestamp();

        if (dt <= DETECTION_RATE) {
            im = (data->image)->getImage();
            obj = data->detector->predict(im);
            (data->obj)->setObjects(obj);
            if (obj->num > 0) data->detector->freeObjects(obj);

            free_image(im);
        }

        rate = (int)((DETECTION_RATE - (what_time_is_it_now()-time_beginning))*1000000);
        if (rate < 0) rate = 0;
        usleep(rate);
    }

    data->is_running = false;

    pthread_exit(NULL);
}

void *process_in_thread(void *ptr) {
    processThreadData *data = (processThreadData*)ptr;

    Detector::objects *front_image_obj;
    Detector::objects *back_image_obj;

    list *front_image_person_cluster, *back_image_person_cluster;
    list *front_image_forklift_cluster, *back_image_forklift_cluster;

    double time_beginning, dt_front_cam, dt_back_cam;
    int rate;

    data->is_running = true;

    while (data->is_running) {
        time_beginning = what_time_is_it_now();

        dt_front_cam = what_time_is_it_now() - (data->front_cam_obj)->getTimestamp();
        dt_back_cam = what_time_is_it_now() - (data->back_cam_obj)->getTimestamp();

        (data->front_cam_image_clusterer)->updateModel(PROCESS_RATE);
        (data->back_cam_image_clusterer)->updateModel(PROCESS_RATE);

        if (dt_front_cam <= PROCESS_RATE) {
            front_image_obj = (data->front_cam_obj)->getObjects();
            (data->front_cam_image_clusterer)->updateMeasurement(front_image_obj, PROCESS_RATE);
            Detector::freeObjects(front_image_obj);
        }

        if (dt_back_cam <= PROCESS_RATE) {
            back_image_obj = (data->back_cam_obj)->getObjects();
            (data->back_cam_image_clusterer)->updateMeasurement(back_image_obj, PROCESS_RATE);
            Detector::freeObjects(back_image_obj);
        }

        front_image_person_cluster = (data->front_cam_image_clusterer)->getCluster((char*)"person");
        front_image_forklift_cluster = (data->front_cam_image_clusterer)->getCluster((char*)"forklift");
        back_image_person_cluster = (data->back_cam_image_clusterer)->getCluster((char*)"person");
        back_image_forklift_cluster = (data->back_cam_image_clusterer)->getCluster((char*)"forklift");
        (data->persons_gate_commander)->insertObjects(front_image_person_cluster, back_image_person_cluster);
        (data->forklifts_gate_commander)->insertObjects(front_image_forklift_cluster, back_image_forklift_cluster);

        free_list_contents(front_image_person_cluster);
        free_list_contents(front_image_forklift_cluster);
        free_list_contents(back_image_person_cluster);
        free_list_contents(back_image_forklift_cluster);
        free_list(front_image_person_cluster);
        free_list(front_image_forklift_cluster);
        free_list(back_image_person_cluster);
        free_list(back_image_forklift_cluster);

        rate = (int)((PROCESS_RATE - (what_time_is_it_now()-time_beginning))*1000000);
        if (rate < 0) rate = 0;
        usleep(rate);
    }

    data->is_running = false;

    pthread_exit(NULL);
}

int main(int argc, char **argv) {
    int front_gpu_i = -1;
    int back_gpu_i = -1;
    void *status;
    SharedImage front_cam_im, back_cam_im;
    SharedObjects front_cam_obj, back_cam_obj;

    grabThreadData grab_front_cam_data, grab_back_cam_data;
    detectThreadData detect_front_cam_data, detect_back_cam_data;
    processThreadData process_data;

    pthread_t grab_front_cam_thread, detect_front_cam_thread;
    pthread_t grab_back_cam_thread, detect_back_cam_thread;
    pthread_t process_thread;

    if(argc < 2) {
        fprintf(stderr, "usage: %s <function>\n", argv[0]);
        return 0;
    }

    front_gpu_i = find_int_arg(argc, argv, (char*)"-i_f", 0);
    back_gpu_i = find_int_arg(argc, argv, (char*)"-i_b", 0);

#ifndef GPU
    front_gpu_i = -1;
    back_gpu_i = -1;
#endif

    Detector front_cam_detector(argv[1]);
    Detector back_cam_detector(argv[1]);
    Clusterer front_image_clusterer;
    Clusterer back_image_clusterer;
    GateCommander persons_gate_commander;
    GateCommander forklifts_gate_commander;
    box b1, b2;

    b1.x = 0.5;
    b1.y = 0.8;
    b1.w = 1;
    b1.h = 0.4;

    b2.x = 0.5;
    b2.y = 0.8;
    b2.w = 1;
    b2.h = 0.4;

    front_cam_detector.loadNetwork(argv[2], argv[3], front_gpu_i);
    front_cam_detector.initializePredictionFilter(3);
    front_cam_detector.setConfidenceThreshold(0.3);
    front_cam_detector.setHierThresh(0.25);
    front_cam_detector.setNms(0.45);

    back_cam_detector.loadNetwork(argv[2], argv[3], back_gpu_i);
    back_cam_detector.initializePredictionFilter(3);
    back_cam_detector.setConfidenceThreshold(0.3);
    back_cam_detector.setHierThresh(0.25);
    back_cam_detector.setNms(0.45);

    front_image_clusterer.setVelocityMovingAverageFilterSize(60);
    front_image_clusterer.setMaxDelay(0.35);
    front_image_clusterer.setMinTime(0.36);
    front_image_clusterer.setMinIou(0.8);

    back_image_clusterer.setVelocityMovingAverageFilterSize(60);
    back_image_clusterer.setMaxDelay(0.35);
    back_image_clusterer.setMinTime(0.36);
    back_image_clusterer.setMinIou(0.8);

    persons_gate_commander.setHotZone(b1, b2);
    persons_gate_commander.setCheckMovementDirection(false);
    persons_gate_commander.setGateInFrontZone(0.5, 1, 1.57, 1, 0.01);
    persons_gate_commander.setGateInBackZone(0.5, 1, 1.57, 1, 0.01);
    persons_gate_commander.setMaxTimeInMiddleEarth(15);
    persons_gate_commander.setCommandMedianFilterSize(9);

    grab_front_cam_data.image = &front_cam_im;
    grab_front_cam_data.video = argv[4];

    grab_back_cam_data.image = &back_cam_im;
    grab_back_cam_data.video = argv[5];

    detect_front_cam_data.image = &front_cam_im;
    detect_front_cam_data.obj = &front_cam_obj;
    detect_front_cam_data.detector = &front_cam_detector;

    detect_back_cam_data.image = &back_cam_im;
    detect_back_cam_data.obj = &back_cam_obj;
    detect_back_cam_data.detector = &back_cam_detector;

    process_data.front_cam_obj = &front_cam_obj;
    process_data.front_cam_image_clusterer = &front_image_clusterer;
    process_data.back_cam_obj = &back_cam_obj;
    process_data.back_cam_image_clusterer = &back_image_clusterer;
    process_data.forklifts_gate_commander = &forklifts_gate_commander;
    process_data.persons_gate_commander = &persons_gate_commander;

    if(pthread_create(&grab_front_cam_thread, 0, grab_in_thread, (void *)&grab_front_cam_data)) error("Front camera grabber thread creation failed!");
    if(pthread_create(&grab_back_cam_thread, 0, grab_in_thread, (void *)&grab_back_cam_data)) error("Back camera grabber thread creation failed!");
    if(pthread_create(&detect_front_cam_thread, 0, detect_in_thread, (void *)&detect_front_cam_data)) error("Front camera detect thread creation failed!");
    if(pthread_create(&detect_back_cam_thread, 0, detect_in_thread, (void *)&detect_back_cam_data)) error("Back camera detect thread creation failed!");
    if(pthread_create(&process_thread, 0, process_in_thread, (void *)&process_data)) error("Process thread creation failed!");


    cv::namedWindow( "w", 1);
    cv::namedWindow( "w2", 1);
    cv::Mat frame_out(720, 1200,16), frame_out2(720, 1200,16);
    image im, im2;
    list *l, *l2;

    double t;

    while(grab_front_cam_data.is_running) {

        if (front_cam_im.getTimestamp() > 0.0) {
            t = what_time_is_it_now();
            im = front_cam_im.getImage();
            im2 = back_cam_im.getImage();

            Detector::object det;

            det.b = b1;
            front_cam_detector.drawDetection(im, det.b,(char*)"front hot_zone", Detector::Blue, 0.006, 0.02);

            det.b = b2;
            back_cam_detector.drawDetection(im2, det.b,(char*)"back hot_zone", Detector::Blue, 0.006, 0.02);

            l = front_image_clusterer.getCluster("person");
            l2 = back_image_clusterer.getCluster("person");
            //printf("Velicina liste %d\n", l->size);
            node *n = l->front;
            node *n2 = l2->front;
            //if (!n) printf("0,0,");
            while(n) {
                Detector::object *ol = (Detector::object*)n->val;
                char id[3];
                char ime[20] = "person_";
                sprintf(id, "%d", ol->id);
                //printf("%f,%f, %f, %f, %f, %f,",ol->vb.x, ol->b.x, ol->vz, ol->z, ol->vb.y, ol->b.y);
                strcat(ime, id);
                front_cam_detector.drawDetection(im, ol->b,ime, Detector::Red, 0.006, 0.02);
                n = n->next;
            }
            while(n2) {
                Detector::object *ol = (Detector::object*)n2->val;
                char id[3];
                char ime[20] = "person_";
                sprintf(id, "%d", ol->id);
                printf("Id %s\n", id);
                //printf("%f,%f, %f, %f, %f, %f,",ol->vb.x, ol->b.x, ol->vz, ol->z, ol->vb.y, ol->b.y);
                strcat(ime, id);
                back_cam_detector.drawDetection(im2, ol->b,ime, Detector::Red, 0.006, 0.02);
                n2 = n2->next;
            }

            imageIntoMat(im, &frame_out);
            imageIntoMat(im2, &frame_out2);
            free_image(im);
            free_image(im2);

            char front_zone_obj[50], back_zone_obj[50];
            sprintf(front_zone_obj,"Number of objects: %d", persons_gate_commander.getNumberOfObjectsInFrontZone());
            sprintf(back_zone_obj,"Number of objects: %d", persons_gate_commander.getNumberOfObjectsInBackZone());
            printf("Broj ljudi front hot_zone %d\n", persons_gate_commander.getNumberOfObjectsInFrontZone());
            printf("Broj ljudi back hot_zone %d\n", persons_gate_commander.getNumberOfObjectsInBackZone());
            printf("Middle earth: %d\n", persons_gate_commander.getNumberOfObjectsInMiddleEarth());
            printf("Command: %d\n", persons_gate_commander.getCommand());
            printf("Filtered Command: %d\n", persons_gate_commander.getFilteredCommand());
            printf("\n");

            if (persons_gate_commander.getFilteredCommand()) {
                cv::putText(frame_out, std::string("Gate: OPEN"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,255,0,255),2);
                cv::putText(frame_out2, std::string("Gate: OPEN"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,255,0,255),2);
            }
            else {
                cv::putText(frame_out, std::string("Gate: CLOSE"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,0,255,255),2);
                cv::putText(frame_out2, std::string("Gate: CLOSE"), cv::Point2f(100,100), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,0,255,255),2);
            }
            cv::putText(frame_out, front_zone_obj, cv::Point2f(100,130), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,0,0,255),2);
            cv::putText(frame_out2, back_zone_obj, cv::Point2f(100,130), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,0,0,255),2);

            cv::imshow("w", frame_out);
            cv::imshow("w2", frame_out2);
            printf("Vrijeme %f\n", what_time_is_it_now()-t);
        }
        cv::waitKey(1);
    }

    grab_front_cam_data.is_running = false;
    grab_back_cam_data.is_running = false;
    detect_front_cam_data.is_running = false;
    detect_back_cam_data.is_running = false;
    process_data.is_running = false;

    /*if (pthread_join(grab_front_cam_thread, &status)) error("Unable to join front camera grabber thread!");
    printf("Grab front\n");
    if (pthread_join(grab_back_cam_thread, &status)) error("Unable to join back camera grabber thread!");
    printf("grab back\n");
    if (pthread_join(detect_front_cam_thread, &status)) error("Unable to join front camera grabber thread!");
    printf("detect front\n");
    if (pthread_join(detect_back_cam_thread, &status)) error("Unable to join back camera detect thread!");
    printf("detect back\n");
    if (pthread_join(process_thread, &status)) error("Unable to join process thread!");
    printf("process\n");*/


    return 0;
}